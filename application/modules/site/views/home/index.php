<?php
$rwelcome = $this->db
->where(COL_CONTENTTYPE,'WelcomeText')
->get(TBL_WEBCONTENT)
->row_array();

$rtestimoni = $this->db
->where(COL_CONTENTTYPE,'Testimonial')
->order_by(COL_UNIQ, 'desc')
->limit(12)
->get(TBL_WEBCONTENT)
->result_array();

$rgaleri = $this->db
->where(COL_CONTENTTYPE,'Galeri')
->get(TBL_WEBCONTENT)
->result_array();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="description" content="<?=$this->setting_web_desc?>">
	<meta name="author" content="Partopi Tao">
	<meta name="keyword" content="chayra, smart, course, partopi tao, psikotest, bimbel, psikotest online, bimbel online">
  <meta property="og:title" content="<?=$this->setting_web_desc.' - Bimbel dan Psikotest Online Kedinasan'?>" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="<?=base_url()?>" />
	<meta property="og:image" content="<?=MY_IMAGEURL.'logo-main.jpeg'?>" />

  <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,500,700,900" rel="stylesheet">
  <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />
  <link rel="icon" type="image/png" href=<?=base_url().$this->setting_web_icon?>>

  <title><?=$this->setting_web_desc.' - Bimbel dan Psikotest Online Kedinasan'?></title>
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/softy/assets/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/themes/softy/assets/css/font-awesome.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/softy/assets/css/templatemo-softy-pinko.css">

  <link href="<?=base_url()?>assets/themes/softy/assets/css/owl.carousel.min.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/softy/assets/css/owl.theme.default.min.css" rel="stylesheet">
  <style>
  .btn-float{
  	position:fixed;
  	width:60px;
  	height:60px;
  	bottom:100px;
  	right:40px;
  	background-color:#25d366;
  	color:#FFF;
  	border-radius:50px;
  	text-align:center;
    font-size:30px;
  	box-shadow: 2px 2px 3px #999;
    z-index:100;
  }

  .my-float{
  	margin-top:16px;
  }

  #form-testimoni input, #form-testimoni textarea {
    color: #777;
    font-size: 14px;
    border: 1px solid #eee;
    width: 100%;
    height: 50px;
    outline: none;
    padding-left: 20px;
    padding-right: 20px;
    border-radius: 25px;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    margin-bottom: 30px;
  }
  .info-box {
    box-shadow: 0 0 1px rgb(0 0 0 / 13%), 0 1px 3px rgb(0 0 0 / 20%);
    border-radius: 0.25rem;
    background: #fff;
    min-height: 80px;
    padding: 0.5rem;
    position: relative;
    margin-bottom: 1rem!important;
    display: -ms-flexbox!important;
    display: flex!important;
  }
  .info-box .info-box-icon {
    border-radius: 0.25rem;
    display: block;
    font-size: 1.875rem;
    text-align: center;
    width: 70px;
    -ms-flex-align: center!important;
    align-items: center!important;
    -ms-flex-pack: center!important;
    justify-content: center!important;
    display: -ms-flexbox!important;
    display: flex!important;
  }
  .elevation-1 {
    box-shadow: 0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24)!important;
  }
  .info-box .info-box-content {
    -ms-flex: 1;
    flex: 1;
    padding: 5px 10px;
  }
  .info-box .info-box-text, .info-box .progress-description {
    display: block;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }
  .info-box .info-box-number {
    display: block;
    font-weight: 700;
  }
  </style>
</head>
<body>
  <!-- ***** Preloader Start ***** -->
  <div id="preloader">
      <div class="jumper">
          <div></div>
          <div></div>
          <div></div>
      </div>
  </div>
  <!-- ***** Preloader End ***** -->
  <!-- ***** Header Area Start ***** -->
  <header class="header-area header-sticky">
      <div class="container">
          <div class="row">
              <div class="col-12">
                  <nav class="main-nav">
                      <!-- ***** Logo Start ***** -->
                      <a href=""<?=site_url()?>"" class="logo">
                        <img src="<?=base_url().$this->setting_web_logo2?>" alt="Logo" style="height: 30px"/>
                      </a>
                      <!-- ***** Logo End ***** -->
                      <!-- ***** Menu Start ***** -->
                      <ul class="nav">
                          <li><a href="#welcome" class="active">Beranda</a></li>
                          <li><a href="#blog">Galeri</a></li>
                          <li><a href="#testimonials">Testimoni</a></li>
                          <li><a href="#contact-us">Kontak</a></li>
                          <li><a href="<?=site_url('daftar')?>">Daftar</a></li>
                      </ul>
                      <a class='menu-trigger'>
                          <span>Menu</span>
                      </a>
                      <!-- ***** Menu End ***** -->
                  </nav>
              </div>
          </div>
      </div>
  </header>
  <!-- ***** Header Area End ***** -->
  <!-- ***** Welcome Area Start ***** -->
  <div class="welcome-area" id="welcome">

      <!-- ***** Header Text Start ***** -->
      <div class="header-text">
          <div class="container">
              <div class="row">
                  <div class="offset-xl-3 col-xl-6 offset-lg-2 col-lg-8 col-md-12 col-sm-12">
                      <h1><?=$this->setting_web_desc?></h1>
                      <p><?=!empty($rwelcome)?$rwelcome[COL_CONTENTDESC1]:''?></p>
                      <a href="<?=site_url('site/user/register')?>" class="main-button-slider"><i class="far fa-user-plus"></i>&nbsp;DAFTAR</a>
                      <small style="color: #fff; padding: 0 10px;">atau</small>
                      <a href="<?=site_url('site/user/login')?>" class="main-button-slider"><i class="far fa-sign-in-alt"></i>&nbsp;MASUK</a>
                  </div>
              </div>
          </div>
      </div>
      <!-- ***** Header Text End ***** -->
  </div>
  <!-- ***** Welcome Area End ***** -->

  <section class="section" id="blog">
      <div class="container">
          <!-- ***** Section Title Start ***** -->
          <div class="row">
              <div class="col-lg-12">
                  <div class="center-heading">
                      <h2 class="section-title">Galeri</h2>
                  </div>
              </div>
          </div>
          <!-- ***** Section Title End ***** -->

          <div class="row">
            <div class="col-12">
              <div id="carouselGaleri" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                  <?php
                  $n=0;
                  for($i=0; $i<count($rgaleri); $i++) {
                    ?>
                    <div class="carousel-item <?=$i==0?'active':''?>">
                      <div class="row">
                        <div class="col-sm-12 text-center">
                          <img src="<?=MY_UPLOADURL.$rgaleri[$i][COL_CONTENTDESC2]?>" style="width: 30vw; box-shadow: 0 2px 48px 0 rgb(0 0 0 / 10%)" alt="<?=$rgaleri[$i][COL_CONTENTTITLE]?>">
                          <p class="font-weight-bold mt-3"><?=$rgaleri[$i][COL_CONTENTTITLE]?><br /><small><?=$rgaleri[$i][COL_CONTENTDESC1]?></small></p>
                        </div>
                      </div>
                    </div>
                    <?php
                  }
                  ?>
                </div>
              </div>
            </div>
          </div>
      </div>
  </section>

  <section class="section" id="testimonials">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="center-heading">
            <h2 class="section-title">Testimoni</h2>
          </div>
        </div>
        <div class="offset-lg-3 col-lg-6">
          <div class="center-text">
            <p>Respon dan kesan-kesan pengguna yang sudah mengunakan layanan yang tersedia di website <?=$this->setting_web_desc?></p>
            <a href="<?=site_url('site/home/testimoni')?>" class="main-button-slider"><i class="far fa-comment-lines"></i>&nbsp;BUAT TESTIMONI</a>
          </div>
        </div>
      </div>
      <div class="row">
        <?php
        foreach($rtestimoni as $t) {
          ?>
          <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="team-item">
              <div class="team-content">
                <i style="margin-left: 0 !important"><img src="<?=base_url()?>assets/themes/softy/assets/images/testimonial-icon.png" alt=""></i>
                <p><?=$t[COL_CONTENTDESC2]?></p>
                <div class="team-info">
                  <h3 class="user-name"><?=strtoupper($t[COL_CONTENTDESC1])?></h3>
                  <span><?=$t[COL_CONTENTTITLE]?></span>
                </div>
              </div>
            </div>
          </div>
          <?php
        }
        ?>
      </div>
    </div>
  </section>

  <!-- ***** Contact Us Start ***** -->
  <section class="section colored" id="contact-us">
      <div class="container">
          <!-- ***** Section Title Start ***** -->
          <div class="row">
              <div class="col-lg-12">
                  <div class="center-heading">
                      <h2 class="section-title">Kontak</h2>
                  </div>
              </div>
              <div class="offset-lg-3 col-lg-6">
                    <div class="center-text">
                        <p>Silakan menghubungi kami melalui informasi kontak dibawah ini.</p>
                    </div>
                </div>
          </div>
          <!-- ***** Section Title End ***** -->

          <div class="row">
              <!-- ***** Contact Text Start ***** -->
              <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="info-box">
                  <span class="info-box-icon elevation-1 text-white" style="background: #6610f2"><i class="fas fa-map-marked-alt"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Alamat</span>
                    <span class="info-box-number">
                      <small class="font-weight-bold"><?=$this->setting_org_address?></small>
                    </span>
                  </div>
                </div>
                <div class="info-box">
                  <span class="info-box-icon elevation-1 text-white" style="background: #6610f2"><i class="fas fa-phone-rotary"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">No.Telp</span>
                    <span class="info-box-number">
                      <small class="font-weight-bold"><?=$this->setting_org_phone?> / <?=$this->setting_org_fax?></small>
                    </span>
                  </div>
                </div>
                <div class="info-box">
                  <span class="info-box-icon elevation-1 text-white" style="background: #6610f2"><i class="fas fa-envelope"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Email</span>
                    <span class="info-box-number">
                      <small class="font-weight-bold"><?=$this->setting_org_mail?></small>
                    </span>
                  </div>
                </div>
              </div>
              <!-- ***** Contact Text End ***** -->

              <!-- ***** Contact Form Start ***** -->
              <div class="col-lg-8 col-md-6 col-sm-12">
                  <?=$this->setting_org_lat?>
              </div>
              <!-- ***** Contact Form End ***** -->
          </div>
      </div>
  </section>
  <!-- ***** Contact Us End ***** -->

  <!-- ***** Footer Start ***** -->
  <footer style="padding-top: 0 !important">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <p class="copyright" style="border-top: none !important; margin-top: 0 !important">Copyright &copy; <?=date('Y')?> <?=$this->setting_web_name?></p>
        </div>
      </div>
    </div>
  </footer>
  <a href="https://api.whatsapp.com/send?phone=<?=$this->setting_org_phone?>&text=Halo" class="btn-float" target="_blank">
    <i class="fa fa-whatsapp my-float"></i>
  </a>

  <!-- jQuery -->
  <script src="<?=base_url()?>assets/themes/softy/assets/js/jquery-2.1.0.min.js"></script>

  <!-- Bootstrap -->
  <script src="<?=base_url()?>assets/themes/softy/assets/js/popper.js"></script>
  <script src="<?=base_url()?>assets/themes/softy/assets/js/bootstrap.min.js"></script>

  <!-- Plugins -->
  <script src="<?=base_url()?>assets/themes/softy/assets/js/scrollreveal.min.js"></script>
  <script src="<?=base_url()?>assets/themes/softy/assets/js/waypoints.min.js"></script>
  <script src="<?=base_url()?>assets/themes/softy/assets/js/jquery.counterup.min.js"></script>
  <script src="<?=base_url()?>assets/themes/softy/assets/js/imgfix.min.js"></script>

  <script src="<?=base_url()?>assets/themes/softy/assets/js/owl.carousel.min.js"></script>

  <!-- Global Init -->
  <script src="<?=base_url()?>assets/themes/softy/assets/js/custom.js"></script>
</body>
</html>
