<?php

?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="<?=isset($ogdesc)?$ogdesc:trim(preg_replace('/\s+/', ' ', $this->setting_web_desc))?>">
  <meta name="author" content="Partopi Tao">
  <meta name="keyword" content="chayra, smart, course, partopi tao, psikotest, bimbel, psikotest online, bimbel online">
  <meta property="og:title" content="<?=isset($ogtitle)?$ogtitle:ucwords(strtolower($this->setting_web_name))?>" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="<?=base_url()?>" />
	<meta property="og:image" content="<?=isset($ogimg)?$ogimg:MY_IMAGEURL.'logo.png'?>" />
  <meta property="og:description" content="<?=isset($ogdesc)?$ogdesc:$this->setting_web_desc?>" />

  <title><?=!empty($title) ? ucwords(strtolower($this->setting_web_name)).' - '.$title.(isset($sub)?' '.$sub:'') : ucwords(strtolower($this->setting_web_name))?></title>

  <link href="<?=base_url()?>assets/themes/gotto/css/fonts.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/gotto/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/gotto/css/bootstrap-icons.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/gotto/css/owl.carousel.min.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/gotto/css/owl.theme.default.min.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/gotto/css/tooplate-gotto-job.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/gotto/css/icheck-bootstrap/icheck-bootstrap.min.css" rel="stylesheet">

  <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />

  <script src="<?=base_url()?>assets/themes/gotto/js/jquery.min.js"></script>
  <script src="<?=base_url()?>assets/themes/gotto/js/bootstrap.min.js"></script>

  <link rel="icon" type="image/png" href=<?=base_url().$this->setting_web_icon?>>
  <style>
  .se-pre-con {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?=base_url().$this->setting_web_preloader?>') center no-repeat #fff;
  }
  .categories-block:hover {
    border-color: var(--secondary-color) !important;
  }
  .btn-float{
  	position:fixed;
  	width:60px;
  	height:60px;
  	bottom:100px;
  	right:40px;
  	background-color:#25d366;
  	color:#FFF;
  	border-radius:50px;
  	text-align:center;
    font-size:30px;
  	box-shadow: 2px 2px 3px #999;
    z-index:100;
  }

  .my-float{
  	margin-top:16px;
  }

  .badge {
    background: var(--white-color);
  }
  .badge:hover {
    border: 1px solid var(--custom-btn-bg-color) !important;
  }

  @media (min-width: 992px) {
    .div-auth {
        padding-right: 5rem!important;
    }

    .info-package {
      display: flex;
      align-items: center;
      justify-content: space-between;
    }
  }
  </style>
</head>
<body id="top">
  <div class="se-pre-con"></div>
  <nav class="navbar navbar-expand-lg">
    <div class="container">
      <a class="navbar-brand d-flex align-items-center" href="<?=site_url()?>">
        <img src="<?=base_url().$this->setting_web_logo2?>" class="img-fluid logo-image" style="width: 180px !important">
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav align-items-center ms-lg-5">
              <li class="nav-item ms-lg-auto">
                  <a class="nav-link active" href="<?=site_url()?>">Beranda</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?=site_url()?>#package">Paket</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?=site_url()?>#galeri">Galeri</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?=site_url()?>#testimonial">Testimoni</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?=site_url()?>#kontak">Kontak</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link custom-btn btn" href="<?=site_url('site/user/register')?>">Daftar</a>
              </li>
          </ul>
      </div>
    </div>
  </nav>
  <main>
    <header class="site-header">
      <div class="section-overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-12 text-center">
            <h1 class="text-white"><?=$title?></h1>
          </div>
        </div>
      </div>
    </header>
    <section class="job-section section-padding" style="background: var(--section-bg-color) !important">
      <div class="container">
        <form id="form-package" action="<?=current_url()?>" action="post">
          <div class="row">
            <div class="col-12 col-lg-6 div-auth">
              <div class="custom-form hero-form" action="<?=current_url()?>" method="post" role="form">
                <h3 class="text-white">Akun Pengguna</h3>
                <p class="text-white mb-4">Silakan daftar akun kamu terlebih dahulu</p>
                <div class="row">
                  <div class="col-12">
                    <div class="input-group">
                      <span class="input-group-text"><i class="bi-person custom-icon"></i></span>
                      <input type="text" name="<?=COL_FULLNAME?>" class="form-control" placeholder="Nama Lengkap" required />
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="input-group">
                      <span class="input-group-text"><i class="bi-at custom-icon"></i></span>
                      <input type="text" name="<?=COL_EMAIL?>" class="form-control" placeholder="Email" required />
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="input-group">
                      <span class="input-group-text"><i class="bi-phone custom-icon"></i></span>
                      <input type="text" name="<?=COL_PHONE?>" class="form-control" placeholder="No. HP" required />
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="input-group">
                      <span class="input-group-text"><i class="bi-key custom-icon"></i></span>
                      <input type="password" name="<?=COL_PASSWORD?>" class="form-control" placeholder="Password" required />
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="input-group">
                      <span class="input-group-text"><i class="bi-key custom-icon"></i></span>
                      <input type="password" name="ConfirmPassword" class="form-control" placeholder="Konfirmasi Password" required />
                    </div>
                  </div>
                  <!--<div class="col-lg-12 col-12">
                    <button type="submit" class="form-control"><i class="far fa-sign-in"></i> Login</button>
                  </div>-->
                  <div class="col-12">
                    <div class="d-flex flex-wrap align-items-center mt-4 mt-lg-0">
                      <span class="text-white mb-lg-0 mb-md-0 me-2">Sudah punya akun?</span>
                      <div>
                        <a href="<?=site_url('site/user/login').'?redir='.urlencode(site_url('site/user/package'))?>" class="badge"><i class="far fa-sign-in"></i>&nbsp;Login</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-6 pt-3 pb-3 mx-auto">
              <?php
              /*if(isset($sub)) {
                ?>
                <h3><?=$sub?></h3>
                <?php
              }*/
              ?>
              <h3 class="p-3 pb-0 mb-2">Daftar Paket</h3>
              <div class="reviews-thumb" style="padding: 20px !important">
                <p class="p-3 pb-0">Berikut ini adalah daftar paket simulasi / ujian berbasis CAT <?=isset($sub)?'kategori <strong>'.$sub.'</strong>':''?> yang tersedia:</p>
                <?php
                foreach($rpkg as $pkg) {
                  $isIncludeACR = false;
                  $arrItems = json_decode($pkg[COL_PKGITEMS]);
                  foreach($arrItems as $i) {
                    $rtest = $this->db
                    ->where(COL_UNIQ, $i->TestID)
                    ->get(TBL_MTEST)
                    ->row_array();

                    if(!empty($rtest) && $rtest[COL_TESTTYPE]=='ACR') {
                      $isIncludeACR = true;
                    }
                  }
                  ?>
                  <div class="contact-info info-package" style="border-radius: var(--border-radius-medium)">
                    <div class="radio icheck-wisteria">
                      <input type="radio" id="chk_<?=$pkg[COL_UNIQ]?>" name="IdPkg" value="<?=$pkg[COL_UNIQ]?>" data-acr="<?=$isIncludeACR?1:0?>" required />
                      <label for="chk_<?=$pkg[COL_UNIQ]?>" style="line-height: 1.75 !important">
                        <?=strtoupper($pkg[COL_PKGNAME])?>
                      </label>
                    </div>
                    <span class="badge d-block" style="border: 1px solid var(--primary-color); font-size: 1rem !important">Rp. <?=number_format($pkg[COL_PKGPRICE])?></span>
                    <!--<span class="badge" style="float: right; border: 1px solid var(--primary-color); font-size: 1rem !important">Rp. <?=number_format($pkg[COL_PKGPRICE])?></span>-->
                  </div>
                  <?php
                }
                ?>
                <div class="custom-form hero-form mt-4 mb-3 d-none">
                  <h5 class="text-white mb-3">OPSI TAMBAHAN</h5>
                  <div class="row">
                    <div class="col-12">
                      <label class="text-white">Urutan Soal</label>
                      <div class="input-group">
                        <span class="input-group-text"><i class="bi-list custom-icon"></i></span>
                        <select class="form-select form-control" name="<?=COL_SESSREMARK1?>">
                          <option value="NORANDOM">SOAL TIDAK DIACAK</option>
                          <option value="">SOAL DIACAK</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-12 div-acropt">
                      <label class="text-white">Kombinasi Soal</label>
                      <div class="input-group">
                        <span class="input-group-text"><i class="bi-list custom-icon"></i></span>
                        <select class="form-select form-control" name="<?=COL_SESSREMARK2?>" placeholder="KOMBINASI">
                          <option value="ANGKA">ANGKA</option>
                          <option value="HURUF">HURUF</option>
                          <option value="SIMBOL">SIMBOL</option>
                          <option value="MIXED">ANGKA & HURUF</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <p class="p-2">
                  <button type="submit" class="custom-btn btn d-block w-100">DAFTAR <i class="far fa-arrow-circle-right"></i></button>
                </p>
              </div>
            </div>
          </div>
        </form>
      </div>
    </section>
  </main>

  <footer class="site-footer" id="kontak">
    <div class="container">
      <div class="row">
        <div class="col-lg-7 col-md-6 col-12 mb-3">
          <div class="d-flex align-items-center mb-4">
            <img src="<?=base_url().$this->setting_web_logo?>" class="img-fluid logo-image">
            <div class="d-flex flex-column">
              <strong class="logo-text">CHAYRA</strong>
              <small class="logo-slogan" style="font-size: 16pt !important">SMART COURSE</small>
            </div>
          </div>
          <p class="mb-2">
            <i class="custom-icon fas fa-map-marked-alt me-1"></i>
            <a href="#" class="site-footer-link"><?=$this->setting_org_address?></a>
          </p>

          <p class="mb-2">
            <i class="custom-icon fas fa-phone-rotary me-1"></i>
            <a href="#" class="site-footer-link"><?=$this->setting_org_phone?> / <?=$this->setting_org_fax?></a>
          </p>

          <p class="mb-2">
            <i class="custom-icon fas fa-envelope me-1"></i>
            <a href="#" class="site-footer-link"><?=$this->setting_org_mail?></a>
          </p>

          <p>
            <i class="custom-icon fab fa-instagram me-1"></i>
            <a href="#" class="site-footer-link">@chayrasmartcourse</a>
          </p>
        </div>

        <div class="col-lg-5 col-md-6 col-12 mt-3 mt-lg-0">
          <?=$this->setting_org_lat?>
        </div>
      </div>
    </div>

    <div class="site-footer-bottom">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-12 d-flex align-items-center">
            <p class="copyright-text">Copyright &copy; <?=date('Y')?> <?=ucwords(strtolower($this->setting_web_name))?></p>
            <ul class="footer-menu d-flex">
              <li class="footer-menu-item"><a href="#" class="footer-menu-link mb-0">Privacy Policy</a></li>
              <li class="footer-menu-item"><a href="#" class="footer-menu-link mb-0">Terms</a></li>
            </ul>
          </div>
          <div class="col-lg-6 col-12 mt-2 mt-lg-0" style="text-align: right">
            <p style="padding-right: 15px">Developed by : <a class="sponsored-link" rel="sponsored" href="https://www.linkedin.com/in/yoelrolas/" target="_blank">Partopi Tao</a></p>
          </div>
          <!--<a class="back-top-icon bi-arrow-up smoothscroll d-flex justify-content-center align-items-center" href="#top"></a>-->
        </div>
      </div>
    </div>
  </footer>
  <a href="https://api.whatsapp.com/send?phone=<?=$this->setting_org_phone?>&text=Halo" class="btn-float" target="_blank">
    <i class="fab fa-whatsapp my-float"></i>
  </a>

  <!-- JAVASCRIPT FILES -->
  <script src="<?=base_url()?>assets/themes/gotto/js/owl.carousel.min.js"></script>
  <script src="<?=base_url()?>assets/themes/gotto/js/counter.js"></script>
  <script src="<?=base_url()?>assets/themes/gotto/js/custom.js"></script>

  <!-- Block UI -->
  <script type="text/javascript" src="<?=base_url() ?>assets/js/jquery.blockUI.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.validate.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/template/js/function.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.form.js"></script>

  <!-- Toastr -->
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.css">
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.js"></script>


  <script type="text/javascript">
  $(window).load(function() {
    $(".se-pre-con").fadeOut("slow");
  });
  $(document).ready(function(){
    $('#form-package').validate({
      submitHandler: function(form) {
        var btnSubmit = $('button[type=submit]', form);
        var txtSubmit = btnSubmit.html();
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);

        $(form).ajaxSubmit({
          dataType: 'json',
          type : 'post',
          success: function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
              btnSubmit.html(txtSubmit).attr('disabled', false);
            } else {
              toastr.success(res.success);
              if(res.redirect) {
                setTimeout(function(){
                  if(res.url) {
                    window.open(res.url);
                  }
                  location.href = res.redirect;
                }, 1000);
              }
            }
          },
          error: function() {
            toastr.error('Maaf, terjadi gangguan pada sistem. Silakan coba beberapa saat lagi atau hubungi administrator.');
          },
          complete: function() {
            btnSubmit.html(txtSubmit);
            btnSubmit.attr('disabled', false);
            $(form).closest('.modal').modal('hide');
          }
        });

        return false;
      }
    });

    $('[name=IdPkg]').change(function(){
      var acr = $('input[name=IdPkg][type=radio]:checked').data('acr');
      if(acr==1) {
        $('[name=SessRemark2]', $('.div-acropt')).attr('disabled', false);
        $('.div-acropt').show();
      } else {
        $('[name=SessRemark2]', $('.div-acropt')).attr('disabled', true);
        $('.div-acropt').hide();
      }
    }).trigger('change');
  });
  </script>
</body>
</html>
