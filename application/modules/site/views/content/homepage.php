<?php
$rwelcome = $this->db
->where(COL_CONTENTTYPE,'WelcomeText')
->get(TBL_WEBCONTENT)
->row_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 float-sm-right">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Dashboard</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <form action="<?=current_url()?>" method="post">
            <div class="card-header">
              <h5 class="card-title">Welcome Text</h5>
            </div>
            <div class="card-body">
              <textarea rows="5" class="ckeditor" name="WelcomeText" placeholder="Masukkan teks pembuka halaman utama (welcome text) disini"><?=!empty($rwelcome)?$rwelcome[COL_CONTENTDESC1]:''?></textarea>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-outline-success"><i class="far fa-check-circle"></i>&nbsp;&nbsp;SIMPAN</button>
            </div>
          </form>
        </div>
        <div class="card card-default">
          <form action="<?=current_url()?>" method="post">
            <div class="card-header">
              <h5 class="card-title">Kontak</h5>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                    <label>EMAIL</label>
                    <input type="email" class="form-control" name="Email" value="<?=$this->setting_org_mail?>" />
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label>NO TELP. 1</label>
                    <input type="text" class="form-control" name="Phone1" value="<?=$this->setting_org_phone?>" />
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label>NO TELP. 2</label>
                    <input type="text" class="form-control" name="Phone2" value="<?=$this->setting_org_fax?>" />
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label>ALAMAT</label>
                <textarea rows="5" class="form-control" name="Address"><?=$this->setting_org_address?></textarea>
              </div>
              <div class="form-group">
                <label>GOOGLE MAPS LINK</label>
                <textarea rows="5" class="form-control" name="GMaps"><?=$this->setting_org_lat?></textarea>
              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-outline-success"><i class="far fa-check-circle"></i>&nbsp;&nbsp;SIMPAN</button>
            </div>
          </form>

        </div>

      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function(){

});
</script>
