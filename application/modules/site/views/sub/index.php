<?php
$rpkg = $this->db
->select('msubscription.*, mkategori.Kategori')
->join(TBL_MKATEGORI,TBL_MKATEGORI.'.'.COL_UNIQ." = ".TBL_MSUBSCRIPTION.".".COL_IDKATEGORI,"left")
->where(TBL_MKATEGORI.'.'.COL_KATEGORIISAKTIF, 1)
->order_by(TBL_MKATEGORI.'.'.COL_KATEGORI)
->get(TBL_MSUBSCRIPTION)
->result_array();

$rusers = $this->db
->query("select * from users where RoleID=".ROLEUSER." order by Fullname")
->result_array();
?>
<style>
#datalist_filter {
  text-align: left !important;
  display: inline-block !important;
}
#datalist_filter label {
  font-weight: 700;
}
#datalist tbody th, #datalist tbody td {
  vertical-align: middle;
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 float-sm-right">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Dashboard</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-header">
            <div class="card-tools text-center" style="float: none !important">
              <a href="<?=site_url('site/subscription/add')?>" type="button" class="btn btn-tool btn-add-data text-primary"><i class="fas fa-plus"></i>&nbsp;TAMBAH</a>
              <button type="button" class="btn btn-tool btn-refresh-data"><i class="fas fa-sync-alt"></i>&nbsp;REFRESH</button>
            </div>
          </div>
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered table-hover table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 10px">#</th>
                    <th>KATEGORI</th>
                    <th>DURASI</th>
                    <th>SATUAN</th>
                    <th>PENGGUNA</th>
                    <th>TGL. MULAI</th>
                    <th>TGL. BERAKHIR</th>
                    <th>DIBUAT PADA</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-add" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">TAMBAH SUBSCRIPTION</span>
      </div>
      <form id="form-sub" action="<?=site_url('site/subscription/add')?>" method="post">
        <div class="modal-body">
          <div class="form-group">
            <div class="row">
              <div class="col-sm-7">
                <label class="control-label">PAKET</label>
                <select class="form-control" name="<?=COL_IDPACKAGE?>" style="width: 100%" required>
                  <?php
                  foreach($rpkg as $p) {
                    ?>
                    <option value="<?=$p[COL_UNIQ]?>" data-dur="<?=$p[COL_SUBSDUR]?>" data-term="<?=ucwords(getEnumPeriod($p[COL_SUBSTERM]))?>" data-price="<?=$p[COL_SUBSPRICE]?>"><?=$p[COL_KATEGORI].' - '.$p[COL_SUBSDUR].' '.ucwords(getEnumPeriod($p[COL_SUBSTERM]))?></option>
                    <?php
                  }
                  ?>
                </select>
              </div>
              <div class="col-sm-5">
                <label class="control-label">TGL. MULAI</label>
                <input type="text" name="<?=COL_SUBSDATEFROM?>" class="form-control datepicker" />
              </div>
            </div>
          </div>
          <div class="form-group d-none">
            <div class="row">
              <div class="col-sm-7">
                <label class="control-label">JANGKA WAKTU</label>
                <div class="input-group">
                  <input type="text" name="<?=COL_SUBSDUR?>" class="form-control uang text-right" readonly />
                  <div class="input-group-append">
                    <span class="input-group-text label-substerm">Hari</span>
                  </div>
                </div>
              </div>
              <div class="col-sm-5">
                <label class="control-label">HARGA</label>
                <input type="text" name="<?=COL_SUBSPRICE?>" class="form-control uang text-right" readonly />
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label">PENGGUNA</label>
            <select class="form-control" name="<?=COL_USERNAME?>" style="width: 100%" required>
              <?php
              foreach($rusers as $u) {
                ?>
                <option value="<?=$u[COL_USERNAME]?>" data-value="<?=$u[COL_EMAIL]?>"><?=$u[COL_FULLNAME]?></option>
                <?php
              }
              ?>
            </select>
          </div>
          <div class="form-group">
            <label class="control-label">AKUN</label>
            <input type="text" name="<?=COL_EMAIL?>" class="form-control" readonly />
          </div>
          <div class="form-group">
            <label class="control-label">CATATAN</label>
            <textarea class="form-control" rows="3" name="<?=COL_SUBSREMARKS?>"></textarea>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
          <button type="submit" class="btn btn-sm btn-outline-success"><i class="far fa-arrow-circle-right"></i>&nbsp;LANJUT</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div id="dom-filter" class="d-none">
  <select class="form-control" name="filterStatus" style="width: 200px">
    <option value="">-- SEMUA STATUS --</option>
    <option value="1">AKTIF</option>
    <option value="-1">BERAKHIR</option>
  </select>
</div>
<script type="text/javascript">
$(document).ready(function() {
  var orderdef = [[ 7, "desc" ]];
  var coldefs = [
    {"targets":[0], "className":'nowrap text-center'},
    {"targets":[2,5,6,7], "className":'nowrap dt-body-right'}
  ];
  var cols = [
    {"orderable": false,"width": "50px"},
    {"orderable": true},
    {"orderable": false,"width": "10px"},
    {"orderable": false,"width": "10px"},
    {"orderable": true},
    {"orderable": true,"width": "100px"},
    {"orderable": true,"width": "100px"},
    {"orderable": true,"width": "10px"}
  ];

  var dt = $('#datalist').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('site/subscription/index-load')?>",
      "type": 'POST',
      "data": function(data){
        data.filterStatus = $('[name=filterStatus]', $('.filtering')).val();
       }
    },
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    "oLanguage": {
      "sSearch": "FILTER "
    },
    "dom":"R<'row'<'col-sm-12 d-flex'f<'filtering'>>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "order": orderdef,
    "columnDefs": coldefs,
    "columns": cols,
    "createdRow": function(row, data, dataIndex) {
      $('.btn-action', $(row)).click(function() {
        var url = $(this).attr('href');
        if(confirm('Apakah anda yakin?')) {
          $.get(url, function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success(res.success);
            }
          }, "json").done(function() {
            dt.DataTable().ajax.reload();
          }).fail(function() {
            toastr.error('SERVER ERROR');
          });
        }
        return false;
      });
      $('[data-toggle="tooltip"]', $(row)).tooltip();
    },
    "initComplete": function(settings, json) {
      $('input[type=search]', $('#datalist_filter')).removeClass('form-control-sm').attr('placeholder', 'Keyword');
    }
  });
  $("div.filtering").html($('#dom-filter').html()).addClass('d-inline-block ml-2');

  $('.btn-refresh-data').click(function() {
    dt.DataTable().ajax.reload();
  });
  $('input,select', $("div.filtering")).change(function() {
    dt.DataTable().ajax.reload();
  });

  $('.btn-add-data').click(function() {
    $('#modal-add').modal('show');
    return false;
  });

  $('[name=Username]').change(function(){
    var email = $('option:selected', $(this)).data('value');

    $('[name=Email]').val(email);
  }).trigger('change');

  $('[name=IDPackage]').change(function(){
    var dur = $('option:selected', $(this)).data('dur');
    var term = $('option:selected', $(this)).data('term');
    var price = $('option:selected', $(this)).data('price');

    $('[name=SubsDur]').val(dur);
    $('[name=SubsPrice]').val(price);
    $('.label-substerm').text(term);
  }).trigger('change');

  $('#form-sub').validate({
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', form);
      var txtSubmit = btnSubmit.html();
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      btnSubmit.attr('disabled', true);

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            dt.DataTable().ajax.reload();
            $('#modal-add').modal('hide');
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>
