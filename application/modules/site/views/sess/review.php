<?php
$rsheet = $this->db
->where(COL_IDTEST, $rtest[COL_UNIQ])
->order_by(COL_UNIQ)
->get(TBL_TSESSIONSHEET)
->result_array();

$rskor = $this->db
->select_sum(COL_QUESTSCORE)
->where(COL_IDTEST, $rtest[COL_UNIQ])
->get(TBL_TSESSIONSHEET)
->row_array();

$repps = array();
if($rtest[COL_TESTREMARKS1]=='EPPS') {
  $repps = $this->db
  ->where(COL_IDSESSION, $rtest[COL_IDSESSION])
  ->where(COL_IDTEST, $rtest[COL_UNIQ])
  ->get(TBL_EPPS_SESSION)
  ->row_array();

  if(!empty($repps)) {
    $rskor = $this->db
    ->where(COL_EPPSSESSID, $repps[COL_UNIQ])
    ->get(TBL_EPPS_SESSIONDET)
    ->result_array();
  }
}
?>
<style>
.card-opacity {
  opacity: 92% !important;
}
</style>
<section class="content" style="background:url('<?=MY_IMAGEURL.'bg-watermark.png'?>')">
  <div class="container">
    <div class="row pt-2">
      <div class="col-sm-12">
        <div class="card card-default mt-2">
          <div class="card-header">
            <h5 class="card-title m-0 font-weight-bold"><?=$rtest[COL_TESTNAME]?></h5>
          </div>
          <div class="card-body p-0">
            <table class="table table-striped table-condensed" style="max-width: 100%">
              <tbody>
                <tr>
                  <td style="width: 10px; white-space: nowrap">NAMA</td>
                  <td style="width: 10px; white-space: nowrap">:</td>
                  <td><strong><?=$rsess[COL_FULLNAME]?></strong></td>
                </tr>
                <tr>
                  <td style="width: 10px; white-space: nowrap">TANGGAL / WAKTU</td>
                  <td style="width: 10px; white-space: nowrap">:</td>
                  <td><i class="far fa-calendar"></i>&nbsp;&nbsp;<strong><?=date('Y-m-d', strtotime($rtest[COL_TESTSTART]))?></strong>&nbsp;&nbsp;&nbsp;&nbsp;<i class="far fa-clock-o"></i>&nbsp;&nbsp;<strong><?=date('H:i', strtotime($rtest[COL_TESTSTART]))?></strong> s.d <strong><?=date('H:i', strtotime($rtest[COL_TESTEND]))?></strong></td>
                </tr>
                <?php
                if($rtest[COL_TESTREMARKS1] != 'EPPS') {
                  ?>
                  <tr>
                    <td style="width: 10px; white-space: nowrap">POIN</td>
                    <td style="width: 10px; white-space: nowrap">:</td>
                    <td><strong><?=!empty($rskor)?number_format($rskor[COL_QUESTSCORE]):'-'?></strong></td>
                  </tr>
                  <tr>
                    <td style="width: 10px; white-space: nowrap">NILAI PSIKOLOGI</td>
                    <td style="width: 10px; white-space: nowrap">:</td>
                    <td><strong><?=isset($rtest[COL_TESTSCORE])?number_format($rtest[COL_TESTSCORE]):'-'?></strong></td>
                  </tr>
                  <?php
                } else {
                  ?>
                  <tr>
                    <td style="width: 10px; white-space: nowrap">KATEGORI</td>
                    <td style="width: 10px; white-space: nowrap">:</td>
                    <td><strong><?=!empty($repps)?$repps[COL_EPPSKODE]:'-'?></strong></td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>

        <?php
        if($rtest[COL_TESTREMARKS1]=='EPPS') {
          $tbl1 = array(0,4);
          $tbl2 = array(5,9);
          $tbl3 = array(10,15);
          ?>
          <div class="card card-default mt-2">
            <div class="card-header">
              <h5 class="card-title m-0 font-weight-bold">Rincian Hasil</h5>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-4">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th class="text-center">Faktor</th>
                        <th class="text-center">Skor</th>
                        <th class="text-center">%</th>
                        <th class="text-center">Kategori</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      for($i=$tbl1[0]; $i<=$tbl1[1]; $i++) {
                        $r = $rskor[$i];
                        ?>
                        <tr>
                          <td class="text-center"><?=$r[COL_EPPSFAKTOR]?></td>
                          <td class="text-center"><?=number_format($r[COL_EPPSSCORE])?></td>
                          <td class="text-center"><?=number_format($r[COL_EPPSPERCENTILE])?></td>
                          <td class="text-center"><?=$r[COL_EPPSCATEGORY]?></td>
                        </tr>
                        <?php
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
                <div class="col-sm-4">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th class="text-center">Faktor</th>
                        <th class="text-center">Skor</th>
                        <th class="text-center">%</th>
                        <th class="text-center">Kategori</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      for($i=$tbl2[0]; $i<=$tbl2[1]; $i++) {
                        $r = $rskor[$i];
                        ?>
                        <tr>
                          <td class="text-center"><?=$r[COL_EPPSFAKTOR]?></td>
                          <td class="text-center"><?=number_format($r[COL_EPPSSCORE])?></td>
                          <td class="text-center"><?=number_format($r[COL_EPPSPERCENTILE])?></td>
                          <td class="text-center"><?=$r[COL_EPPSCATEGORY]?></td>
                        </tr>
                        <?php
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
                <div class="col-sm-4">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th class="text-center">Faktor</th>
                        <th class="text-center">Skor</th>
                        <th class="text-center">%</th>
                        <th class="text-center">Kategori</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      for($i=$tbl3[0]; $i<=$tbl3[1]; $i++) {
                        $r = $rskor[$i];
                        ?>
                        <tr>
                          <td class="text-center"><?=$r[COL_EPPSFAKTOR]?></td>
                          <td class="text-center"><?=number_format($r[COL_EPPSSCORE])?></td>
                          <td class="text-center"><?=number_format($r[COL_EPPSPERCENTILE])?></td>
                          <td class="text-center"><?=$r[COL_EPPSCATEGORY]?></td>
                        </tr>
                        <?php
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <?php
        }
        ?>

        <?php
        $no=1;
        foreach($rsheet as $s) {
          $key='';
          $isTrue = false;
          $classCard = 'card-default';
          $classBg = 'bg-secondary';

          $optArr = json_decode($s[COL_QUESTOPTION]);
          $ptMax = 0;
          $ptMin = 0;
          foreach($optArr as $opt) {
            if($opt->Val>$ptMax) {
              $ptMax=$opt->Val;
              $key=$opt->Txt;
            }

            if($opt->Val<$ptMin) $ptMin=$opt->Val;
          }

          if ($rtest[COL_TESTREMARKS1]!='EPPS') {
            if($s[COL_QUESTSCORE]==$ptMax) {
              $isTrue = true;
              $classCard = 'card-success';
              $classBg = 'bg-success';
            }
            else if($s[COL_QUESTSCORE]==$ptMin) {
              $classCard = 'card-danger';
              $classBg = 'bg-danger';
            }
          }
          ?>
          <div class="card card-outline <?=$classCard?> card-opacity">
            <div class="card-header">
              <h3 class="card-title">
                <span class="badge <?=$classBg?>"><?=$no?></span>
              </h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <?php
              if($rtest[COL_TESTTYPE] != 'ACR') {
                if(!empty($s[COL_QUESTIMAGE])) {
                  $arrImg = explode(",", $s[COL_QUESTIMAGE]);
                  if(count($arrImg)>0) {
                    foreach($arrImg as $img) {
                      ?>
                      <img class="p-2" src="<?=MY_UPLOADURL.$img?>" style="max-width: 50%" />
                      <?php
                    }
                  }
                }
                ?>
                <?=$s[COL_QUESTTEXT]?>
                <?php
                if(!empty($s[COL_QUESTTEXTSUB])) {
                  if($rtest[COL_TESTTYPE]=='PAULI') {
                    echo ' : <strong>'.str_replace(",",' '.$rtest[COL_TESTSYMBOL].' ',$s[COL_QUESTTEXTSUB]).'</strong>';
                  }

                }
                ?>
                <div class="clearfix"></div>
                <?php
                if($s[COL_QUESTTYPE]=='MUL') {
                  ?>
                  <div class="row mt-4 pl-2 pr-2 div-opt">
                    <?php
                    foreach($optArr as $opt) {
                      $classOpt = 'secondary';

                      if ($rtest[COL_TESTREMARKS1]!='EPPS') {
                        if($opt->Val == $ptMax) $classOpt = 'success';
                        else if(!$isTrue && $opt->Opt == $s[COL_QUESTRESPONSE]) {
                          $classOpt = 'danger';
                        }
                      } else {
                        if($opt->Opt == $s[COL_QUESTRESPONSE]) {
                          $classOpt = 'success';
                        }
                      }
                      ?>
                      <div class="<?=$rtest[COL_TESTTYPE]!='ACR'?'col-lg-12 col-12':'col-lg-2 col-2'?> mb-3">
                        <button type="button" data-val="<?=$opt->Opt?>" class="btn btn-block btn-opt btn-outline-<?=$classOpt?> <?=$rtest[COL_TESTTYPE]!='ACR'?'text-left':'text-center'?>" style="position: relative !important; padding: 10px 20px !important">
                          <span class="badge badge-opt bg-<?=$classOpt?>"><?=$opt->Opt?></span>
                          <span class="badge badge-weight bg-<?=$classOpt?>"><?=$opt->Val?></span>
                          <?=$opt->Txt?>
                        </button>
                      </div>
                      <?php
                    }
                    ?>
                  </div>
                  <?php
                } else if ($s[COL_QUESTTYPE]=='TEXT') {
                  ?>
                  <div class="form-group row">
                    <label class="control-label col-sm-2">Jawaban :</label>
                    <div class="col-sm-6">
                      <input type="text" class="form-control <?=$isTrue?'is-valid':'is-invalid'?>" value="<?=$s[COL_QUESTRESPONSE]?>" readonly  />
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="control-label col-sm-2">Kunci :</label>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" value="<?=$key?>" readonly />
                    </div>
                  </div>
                  <?php
                }
              } else {
                ?>
                <div class="row pl-2 pr-2">
                  <?php
                  foreach($optArr as $opt) {
                    ?>
                    <div class="mb-2 mr-2" style="min-width: 25px">
                      <p class="text-center mb-0 font-weight-bold">
                        <?=$opt->Opt?>
                      </p>
                      <button type="button" data-val="<?=$opt->Opt?>" class="btn btn-block btn-opt btn-outline-secondary text-center">
                        <?php
                        if($rsess[COL_SESSREMARK2]=='SIMBOL') {
                          ?>
                          <img src="<?=MY_IMAGEURL.'symbols/'.$opt->Txt?>" style="width:25px; height: 25px" />
                          <?php
                        } else {
                          echo $opt->Txt;
                        }
                        ?>
                      </button>
                    </div>
                    <?php
                  }
                  ?>
                </div>
                <div class="row mt-1 pl-2 pr-2">
                  <?php
                  $arrTxtSub = explode(",",$s[COL_QUESTTEXTSUB]);
                  foreach($arrTxtSub as $txt) {
                    ?>
                    <div class="mb-3 mr-2" style="min-width: 25px">
                      <button type="button" class="btn btn-block btn-outline-secondary text-center">
                        <?php
                        if($rsess[COL_SESSREMARK2]=='SIMBOL') {
                          ?>
                          <img src="<?=MY_IMAGEURL.'symbols/'.$txt?>" style="width:25px; height: 25px" />
                          <?php
                        } else {
                          echo $txt;
                        }
                        ?>
                      </button>
                    </div>
                    <?php
                  }
                  ?>
                </div>
                <div class="row mt-4 pl-2 pr-2 div-opt">
                  <?php
                  foreach($optArr as $opt) {
                    $classOpt = 'secondary';
                    if($opt->Val == $ptMax) $classOpt = 'success';
                    else if(!$isTrue && $opt->Opt == $s[COL_QUESTRESPONSE]) {
                      $classOpt = 'danger';
                    }
                    ?>
                    <div class="mb-2 mr-2" style="min-width: 25px">
                      <button type="button" data-val="<?=$opt->Opt?>" class="btn btn-block btn-opt btn-<?=$classOpt?> text-center">
                        <?=$opt->Opt?>
                      </button>
                    </div>
                    <?php
                  }
                  ?>
                </div>
                <?php
              }
              ?>
            </div>
          </div>
          <?php
          $no++;
        }
        ?>
      </div>

    </div>
  </div>
</section>
