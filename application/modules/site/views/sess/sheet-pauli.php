<?php
$ruser = GetLoggedUser();
$displayname = $ruser ? $ruser[COL_FULLNAME] : "Guest";
$displaypicture = MY_IMAGEURL.'icon-user.jpg';
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title><?=!empty($title) ? $title.' - '.$this->setting_web_name : $this->setting_web_name?></title>
  <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
  <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />
  <link href="<?=base_url()?>assets/tbs/css/ionicons.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/dist/css/adminlte.min.css">
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/modernizr/modernizr.js"></script>
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/iCheck/all.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/datatable/media/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/css/dataTables.bootstrap4.css">

  <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/jquery.dataTables.min.js?ver=1"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/dataTables.bootstrap.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
  <link href="<?=base_url()?>assets/datatable/ext/responsive/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/responsive/js/dataTables.responsive.min.js"></script>
  <script src="<?=base_url()?>assets/js/moment.js"></script>
  <script src="<?=base_url()?>assets/js/countdown.js"></script>
  <script src="<?=base_url()?>assets/js/moment-countdown.js"></script>
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.css">
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.js"></script>
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <link rel="icon" type="image/png" href=<?=base_url().$this->setting_web_icon?>>

  <style>
  .no-js #loader { display: none;  }
  .js #loader { display: block; position: absolute; left: 100px; top: 0; }
  .se-pre-con {
      position: fixed;
      left: 0px;
      top: 0px;
      width: 100%;
      height: 100%;
      z-index: 9999;
      background: url(<?=base_url().$this->setting_web_preloader?>) center no-repeat #fff;
  }

  @media (max-width: 767px) {
    .sidebar-toggle {
      font-size: 3vw !important;
    }
  }

  .form-group .control-label {
      text-align: right;
      line-height: 2;
  }
  .nowrap {
    white-space: nowrap;
  }
  .va-middle {
    vertical-align: middle !important;
  }
  .border-0 td {
    border: 0!important;
  }
  td.dt-body-right {
    text-align: right !important;
  }
  .sidebar-dark-orange .nav-sidebar>.nav-item>.nav-link.active, .sidebar-light-orange .nav-sidebar>.nav-item>.nav-link.active {
    color: #fff !important;
  }
  div.tooltip-inner {
    max-width: 350px !important;
    text-align: left !important;
  }
  .table-condensed td, .table-condensed th {
    padding: .5rem !important;
  }
  span.badge-opt {
    font-size: 16px !important;
    font-weight: bold !important;
    position: absolute !important;
    left: -10px !important;
    top: -7px !important;
  }

  .div-quest img {
    max-width: 100% !important;
  }
  </style>
  <script>
  // Wait for window load
  function startTime() {
    var today = new Date();
    $('#datetime').html(moment(today).format('DD')+' '+moment(today).format('MMM')+' '+moment(today).format('Y')+' <span class="text-muted font-weight-bold">'+moment(today).format('hh')+':'+moment(today).format('mm')+':'+moment(today).format('ss')+'</span>');
    var t = setTimeout(startTime, 1000);
  }
  $(document).ready(function() {
    startTime();
  });
  $(window).load(function() {
      $(".se-pre-con").fadeOut("slow");
  });
  </script>
</head>
<body class="hold-transition layout-top-nav">
  <div class="se-pre-con"></div>
  <div class="wrapper">
    <nav class="main-header navbar navbar-expand navbar-light navbar-white">
      <div class="container">
        <a href="<?=site_url()?>" class="navbar-brand">
          <img src="<?=base_url().$this->setting_web_logo?>" alt="Logo" class="brand-image img-circle">
          <span class="brand-text font-weight-light d-none d-sm-inline-block"><?=$this->setting_web_desc?></span>
        </a>
        <!--<ul class="navbar-nav">
          <li class="nav-item"></li>
          <li class="nav-item d-none d-sm-inline-block">
            <a href="index3.html" class="nav-link">Home</a>
          </li>
        </ul>-->
        <ul class="navbar-nav ml-auto">
          <li class="nav-item pr-2">
            <a class="btn btn-sm btn-success" href="<?=site_url('site/user/dashboard')?>"><i class="fas fa-home"></i><span class="d-none d-sm-inline-block">&nbsp;DASHBOARD</span></a>
          </li>
        </ul>
      </div>
    </nav>
    <div class="content-wrapper">
      <div class="content pt-3">
        <div class="container">
          <div class="row">
            <div class="col-lg-12 col-12">
              <div id="card-pauli" class="card card-outline card-success">
                <div class="card-header">
                  <h5 class="card-title m-0 pt-2 text-center font-weight-bold">
                    <?=strtoupper($rtestCurrent[COL_TESTNAME])?>
                  </h5>
                  <div class="card-tools">
                    <span class="timer p-1 font-weight-bold float-right" style="border: 1px solid #000"></span>
                  </div>
                </div>
                <div class="card-body d-flex">
                  <form id="form-sheet" action="<?=site_url('site/sess/submit/'.$rtestCurrent[COL_UNIQ])?>">
                    <input type="hidden" name="finish" value="0" />
                    <?php
                    $uniqlast = 0;
                    $txt_ = '';
                    $txtGrp_ = 0;
                    $idx = 0;

                    $arrKolom = array();
                    $arrQuest = array();
                    for($nq=0; $nq<count($rquest); $nq++,$idx++) {
                      $q = $rquest[$nq];
                      $opts = json_decode($q[COL_QUESTOPTION]);

                      if($txt_ != $q[COL_QUESTTEXT]) {
                        $txtGrp_++;
                        $txt_ = $q[COL_QUESTTEXT];
                      }

                      $arrKolom[$q[COL_QUESTTEXT]][] = array(COL_UNIQ=>$q[COL_UNIQ], COL_QUESTTEXTSUB=>$q[COL_QUESTTEXTSUB]);
                      ?>
                      <div class="div-quest <?=$nq>0?'d-none':''?>" data-uniq="<?=$q[COL_UNIQ]?>" style="max-width: 100%" data-acrno="<?=$txtGrp_?>">
                        <input type="hidden" name="QuestResponse__<?=$q[COL_UNIQ]?>" />
                      </div>
                      <?php
                      $uniqlast = $q[COL_UNIQ];
                    }
                    ?>
                  </form>

                  <div class="row" style="flex: 10 1 auto">
                    <?php
                    $seqKolom = 0;
                    $jsonKolom = array();
                    $arrPauli = array();
                    foreach($arrKolom as $idx=>$kol) {
                      $jsonKolom[$seqKolom] = $arrKolom[$idx];
                      ?>
                      <!--<div class="p-2" style="flex-grow: 1">
                        <?php
                        $arrQuest = $arrKolom[$idx];
                        $seqQuest = 0;
                        $html = '';
                        foreach($arrQuest as $n=>$q) {
                          $arrnum = explode(",",$q[COL_QUESTTEXTSUB]);

                          $arrPauli[] = array_merge(array('Kolom'=>$seqKolom),$q);
                          if($seqQuest==0) {
                            $html = '<button type="button" class="btn btn-secondary btn-block p-3 mb-3 text-center text-lg font-weight-bold disabled" data-uniq="0" data-kol="'.$seqKolom.'" data-next="'.$q[COL_UNIQ].'">'.$arrnum[0].'</button>'.$html;
                          }
                          $html = '<button type="button" class="btn btn-secondary btn-block p-3 mb-3 text-center text-lg font-weight-bold disabled" data-uniq="'.$q[COL_UNIQ].'" data-kol="'.$seqKolom.'" data-next="'.(!empty($arrQuest[$n+1][COL_UNIQ])?$arrQuest[$n+1][COL_UNIQ]:'').'">'.$arrnum[1].'</button>'.$html;
                          $seqQuest++;
                        }
                        echo $html;
                        ?>
                      </div>-->
                      <?php
                      $seqKolom++;
                    }
                    ?>

                    <?php
                    $kol=0;
                    for($i=0; $i<min($rtestCurrent[COL_TESTQUESTNUM],12); $i++) {
                      ?>
                      <div class="m-1" style="flex-direction: column; justify-content: flex-start; align-items: center; flex: 1">
                        <?php
                        if($i==1) {
                          ?>
                          <input type="number" id="txt-ans" class="form-control text-center angka mt-4 font-weight-bold text-lg" style="height: 5rem" />
                          <?php
                        } else {
                          for($n=0;$n<7;$n++) {
                            ?>
                            <button type="button" class="btn btn-outline-secondary btn-block p-3 text-center text-lg font-weight-bold" data-kol="<?=$kol?>">X</button>
                            <?php
                          }
                          $kol++;
                        }
                        ?>
                      </div>
                      <?php
                    }
                    ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--<footer class="main-footer text-center">
      <strong>Copyright &copy; <?=$this->setting_org_name?>.</strong> Strongly developed by <strong>Partopi Tao</strong>.
    </footer>-->
  </div>
  <div class="modal fade" id="modal-pause" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-body">
          <p class="text-center mb-0">PINDAH KOLOM</p>
        </div>
      </div>
    </div>
  </div>
</body>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/dist/js/adminlte.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/fastclick/fastclick.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/sparklines/sparkline.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/iCheck/icheck.min.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/js/select2.full.min.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap-select.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/template/js/function.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.form.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.number.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/sweetalert.min.js"></script>
<script>
var noACR = 1;
var cd = null;
var pauliKolom = 0;
var pauliUniq = 0;
var pauliArr = <?=json_encode($jsonKolom)?>;
//var pauliJson = <?=json_encode($arrPauli)?>;
function responseChanged(doSubmit=true) {
  var respEl = $('input[name^=QuestResponse__]');
  for (i=0; i<respEl.length; i++) {
    var resp = $(respEl[i]).val();

    if(resp) {
      var questUniq = 0;
      var questEl = $(respEl[i]).closest('.div-quest');

      if(questEl) {
        questUniq = questEl.data('uniq');
        $('.btn-opt', questEl).addClass('btn-outline-secondary').removeClass('btn-outline-success');
        $('.btn-opt[data-val="'+resp+'"]', questEl).addClass('btn-outline-success').removeClass('btn-outline-secondary');

        $('.badge-opt', questEl).addClass('bg-secondary').removeClass('bg-success');
        $('.badge-opt', $('.btn-opt[data-val="'+resp+'"]', questEl)).addClass('bg-success').removeClass('bg-secondary');

        $('.btn-nav-quest[data-target="'+questUniq+'"]').addClass('btn-success').removeClass('btn-secondary');

        $('input.txt-response', questEl).addClass('is-valid');
        $('input.txt-response', questEl).val(resp);
      }
    }
  }

  if(doSubmit===true) {
    $('#form-sheet').ajaxSubmit({
      dataType: 'json',
      type : 'post',
      success: function(res) {
        // do nothing
      },
      error: function() {
        // do nothing
      },
      complete: function() {
        // do nothing
      }
    });
  }
}

function countdownACR(minutes, cbFinished) {
  //console.log('acrno', noACR);
  var elACR = $('div[data-acrno='+noACR+']:first');
  if(elACR.length>0) {
    if(noACR>1) {
      //$('#modal-pause').modal('show');
      //console.log('noACR',noACR);
      /*setTimeout(function(){
        $('#modal-pause').modal('hide');
      }, 1500);*/
    }

    var uniq = elACR.data('uniq');
    $('.btn-nav-quest[data-target='+uniq+']').click();

    shiftPauli((noACR==1));

    noACR++;
    var nowdate = new Date();
    var newdate = new Date();
    newdate.setSeconds(newdate.getSeconds()+(minutes*60));
    cd = countdown(
      function(ts) {
        var dateStart = ts.start.getTime();
        var dateEnd = ts.end.getTime();
        //console.log('ts run', ts);
        //console.log('ts start', dateStart);
        //console.log('ts end', dateEnd);
        $('#timer').html('<strong>'+ts.hours.toString().padStart(2, '0')+'</strong> : <strong>'+ts.minutes.toString().padStart(2, '0')+'</strong> : <strong>'+ts.seconds.toString().padStart(2, '0')+'</strong>');
        $('.timer').html('<strong>'+ts.hours.toString().padStart(2, '0')+'</strong> : <strong>'+ts.minutes.toString().padStart(2, '0')+'</strong> : <strong>'+ts.seconds.toString().padStart(2, '0')+'</strong>');
        if(/*ts.hours==0 && ts.minutes==0 && ts.seconds==0*/ dateStart>=dateEnd) {
          window.clearInterval(cd);
          $('#timer').html('<strong class="text-danger">HABIS</strong>');
          $('.timer').html('<strong class="text-danger">HABIS</strong>');
          countdownACR(minutes, cbFinished);
        }
      },
      newdate,
      countdown.DEFAULTS
    );
    //console.log('cd init', cd);
  } else {
    //console.log('cb finished');
    cbFinished();
  }
}

function shiftPauli(init=false) {
  if(init===false) pauliKolom++;

  console.log('Shift kolom: ',pauliKolom);
  for(kol=0; kol<pauliArr.length; kol++) {
    var kolomEl = $('button[data-kol='+kol+']');

    if(kolomEl.length>0) {
      for(var k=0; k<kolomEl.length; k++) {
        var kolomObj = pauliArr[pauliKolom+kol];
        var questObj = null;

        if(kolomObj && kolomObj[k]) {
          questObj = kolomObj[k];
        }
        if(questObj && questObj.QuestTextSub) {
          var nums = questObj.QuestTextSub.split(',');
          $(kolomEl[k]).text(nums[0]);
          $(kolomEl[k]).attr('data-uniq', questObj.Uniq);

          if(kolomObj[k+1]) {
            $(kolomEl[k]).attr('data-next', kolomObj[k+1].Uniq);
          }

          if(kol==0 && k==0) {
            pauliUniq = questObj.Uniq;
            console.log('Uniq: ', pauliUniq);
          }
        } else {
          $(kolomEl[k]).text('X');
          $(kolomEl[k]).removeClass('btn-outline-secondary');
          $(kolomEl[k]).addClass('btn-secondary');
          $(kolomEl[k]).removeAttr('data-uniq');
          $(kolomEl[k]).removeAttr('data-next');
        }
      }
    }
  }
}

function nextPauli() {
  console.log('Current kolom: ', pauliKolom);
  console.log('Current uniq: ', pauliUniq);

  var currKolom = $('button[data-kol=0]');
  var currEl = $('button[data-uniq='+pauliUniq+']');

  if(currKolom && currEl) {
    var next = currEl.attr('data-next');
    var nextEl = $('button[data-uniq='+next+']');

    for(var k=0; k<currKolom.length; k++) {
      var kolomObj = pauliArr[pauliKolom];
      var questObj = null;
      var nextObj = null;

      if(kolomObj) {
        questObj = kolomObj.filter(function(x) { return x.Uniq == next});
        if(questObj && questObj.length>0) {
          questObj = questObj[0];

          var idxObj = kolomObj.indexOf(questObj);
          if (idxObj >= 0 && idxObj < kolomObj.length - 1) {
            nextObj = kolomObj[idxObj+1];
          }
        }
      }
      if(questObj && questObj.QuestTextSub) {
        var nums = questObj.QuestTextSub.split(',');
        $(currKolom[k]).text(nums[0]);
        $(currKolom[k]).attr('data-uniq', questObj.Uniq);

        if(nextObj) {
          next = nextObj.Uniq;
          $(currKolom[k]).attr('data-next', next);
        }

        if(k==0) {
          pauliUniq = questObj.Uniq;
        }
      }
    }
  }
}

/*function initPauli() {
  for(kol=0; kol<pauliArr.length; kol++) {
    var kolomEl = $('button[data-kol='+kol+']');

    if(kolomEl.length>0) {
      console.log('Init kolom: ',kol);
      for(var k=0; k<kolomEl.length; k++) {
        var questObj = pauliArr[kol][k];
        if(questObj && questObj.QuestTextSub) {
          var nums = questObj.QuestTextSub.split(',');
          $(kolomEl[k]).text(nums[0]);

          if(kol==0 && k==0) {
            pauliUniq = questObj.Uniq;
            console.log('Uniq: ', pauliUniq);
          }
        }
      }
    }
  }
}*/

$(document).ready(function(){
  $('input#txt-ans').focus();
  $('.dropdown-menu textarea,.dropdown-menu input, .dropdown-menu label').click(function(e) {
      e.stopPropagation();
  });
  $(document).on('keypress','.angka',function(e){
    if((e.which <= 57 && e.which >= 48) || (e.keyCode >= 37 && e.keyCode <= 40) || e.keyCode==9 || e.which==43 || e.which==44 || e.which==45 || e.which==46 || e.keyCode==8){
        return true;
    }else{
        return false;
    }
  });
  /*$( ".alert-dismissible" ).fadeOut(3000, function() {

  });*/

  <?php
  if(!empty($rtestCurrent)) {
    ?>
    //console.log('noACR',noACR);
    countdownACR(<?=$rtestCurrent[COL_TESTDURATION]?>, function(){
      //alert('WAKTU TELAH HABIS.');
      $('#form-sheet').submit();
    });
    //initPauli();
    <?php
  }
  ?>

  $('input[name^=QuestResponse__]').change(function() {
    responseChanged();
  });

  responseChanged(false);

  $(document).keyup(function(e){
    console.log(e.key);
    if((e.which <= 57 && e.which >= 48) || (e.keyCode >= 37 && e.keyCode <= 40) || e.keyCode==9 || e.which==43 || e.which==44 || e.which==45 || e.which==46 || e.keyCode==8){
      console.log('Current : ', pauliUniq);
      $('input[name=QuestResponse__'+pauliUniq+']').val(e.key).trigger('change');
      $('input#txt-ans').val(e.key);
      setTimeout(function(){
        $('input#txt-ans').val('').focus;
      }, 100);
      nextPauli();
    } else {
      return false;
    }
  });

  $('#form-sheet').validate({
    submitHandler: function(form) {
      var emptyEl = $('input[name^=QuestResponse__]', form).filter(function() {
        return !this.value;
      });

      var btnSubmit = $('button[type=submit]', $('#form-sheet'));
      var txtSubmit = btnSubmit.innerHTML;
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      btnSubmit.attr('disabled', true);

      $('input[name=finish]', form).val(1);
      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>
</html>
