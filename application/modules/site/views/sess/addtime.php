<form id="form-main" action="<?=current_url()?>" method="post">
  <div class="form-group">
    <label>Pilih Durasi Tambahan Waktu</label>
    <select class="form-control" name="time" style="width: 100%" required>
      <option value="15">15 MENIT</option>
      <option value="30">30 MENIT</option>
      <option value="60">60 MENIT</option>
      <option value="90">90 MENIT</option>
      <option value="120">120 MENIT</option>
    </select>
  </div>
  <div class="form-group text-right mt-3">
    <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
    <button type="submit" class="btn btn-sm btn-outline-success">SUBMIT&nbsp;<i class="far fa-arrow-circle-right"></i></button>
  </div>
</form>
