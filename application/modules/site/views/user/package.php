<?php
$ruser = GetLoggedUser();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row align-items-stretch">
      <?php
      if(!empty($data)) {
        foreach($data as $dat) {
          $rsubs = $this->db
          ->select('tsubscription.*, mkategori.Kategori')
          ->join(TBL_MKATEGORI,TBL_MKATEGORI.'.'.COL_UNIQ." = ".TBL_TSUBSCRIPTION.".".COL_IDKATEGORI,"left")
          ->where(COL_IDKATEGORI, $dat[COL_UNIQ])
          ->where(COL_SUBSDATETO.' >= ', date('Y-m-d'))
          ->where(COL_USERNAME, $ruser[COL_USERNAME])
          ->get(TBL_TSUBSCRIPTION)
          ->row_array();

          $rpkgs = $this->db->select('mtestpackage.*, mkategori.Kategori')
          ->join(TBL_MKATEGORI,TBL_MKATEGORI.'.'.COL_UNIQ." = ".TBL_MTESTPACKAGE.".".COL_IDKATEGORI,"left")
          ->where((!empty($dat[COL_UNIQ])?'mtestpackage.IdKategori='.$dat[COL_UNIQ]:'mtestpackage.IdKategori is null'))
          ->where(COL_PKGISACTIVE, 1)
          ->order_by('mkategori.Kategori', 'asc')
          ->order_by(COL_PKGNAME, 'asc')
          ->get(TBL_MTESTPACKAGE)
          ->result_array();
          ?>
          <div class="col-12 col-sm-12 d-flex align-items-stretch">
            <div class="card card-indigo w-100">
              <div class="card-header">
                <h3 class="card-title font-weight-bold"><?=$dat[COL_KATEGORI]?></h3>
                <?php
                if(!empty($rsubs)) {
                  ?>
                  <div class="card-tools mr-2">
                    <span class="badge badge-warning"><i class="far fa-crown"></i></span>
                  </div>
                  <?php
                }
                ?>

              </div>
              <div class="card-body p-0 table-responsive">
                <table class="table table-hover" width="100%">
                  <tbody>
                    <?php
                    $n=0;
                    foreach($rpkgs as $pkg) {
                      $txt = urlencode("Saya ingin mendapatkan akses simulasi *".strtoupper($pkg[COL_PKGNAME])."* di ".$this->setting_org_name."\n\nBerikut info akun saya:\nNama: *".$ruser[COL_FULLNAME]."*\nUsername: *".$ruser[COL_USERNAME]."*");
                      ?>
                      <tr <?=$n>=4?'class="d-none"':''?> style="line-height: 1.25rem">
                        <td><?=$pkg[COL_PKGNAME]?><br /><small class="font-italic d-block d-sm-none"><?=!empty($rsubs)?'':'Rp. '.number_format($pkg[COL_PKGPRICE])?></small></td>
                        <td class="text-left d-none d-sm-table-cell"><?=!empty($pkg[COL_PKGDESC])?'<small class="font-italic">'.$pkg[COL_PKGDESC].'</small>':''?></td>
                        <td class="text-right d-none d-sm-table-cell"><?=!empty($rsubs)?'':'Rp. '.number_format($pkg[COL_PKGPRICE])?></td>
                        <td style="width: 10px; white-space: nowrap">
                          <a href="<?=site_url('site/user/package-add/'.$pkg[COL_UNIQ])?>" class="btn btn-sm btn-outline-info btn-select">PILIH&nbsp;<i class="far fa-arrow-circle-right"></i></a>
                          <?php
                          /*if(!empty($rsubs)) {
                            ?>
                            <a href="<?=site_url('site/user/package-add/'.$pkg[COL_UNIQ])?>" class="btn btn-sm btn-outline-primary btn-select">PILIH&nbsp;<i class="far fa-arrow-circle-right"></i></a>
                            <?php
                          } else {
                            ?>
                            <a href="https://api.whatsapp.com/send?phone=<?=$this->setting_org_phone?>&text=<?=$txt?>" target="_blank" class="btn btn-sm btn-outline-primary">PILIH&nbsp;<i class="far fa-arrow-circle-right"></i></a>
                            <?php
                          }*/
                          ?>
                        </td>
                      </tr>
                      <?php
                      $n++;
                    }
                    if(count($rpkgs)>4) {
                      ?>
                      <tr>
                        <td colspan="4" class="text-center font-italic"><a href="#" class="btn-pkg-collapse">LIHAT SEMUA (<?=count($rpkgs)?>)</a></td>
                      </tr>
                      <?php
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <?php
        }
      } else {
        ?>
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <p class="text-center mb-0 font-italic">
                Maaf, belum ada data tersedia saat ini.
              </p>
            </div>
          </div>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-add" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">SIMULASI CAT</span>
      </div>
      <div class="modal-body"></div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){
  $('.btn-pkg-collapse').click(function(){
    var tbl = $(this).closest('table');
    $('tr.d-none', tbl).removeClass('d-none');
    $(this).closest('tr').addClass('d-none')
    return false;
  });

  $('.btn-select').click(function(){
    var url = $(this).attr('href');
    $('.modal-body', $('#modal-add')).load(url, function(){
      $('#modal-add').modal('show');
      $('#form-session', $('#modal-add')).validate({
        submitHandler: function(form) {
          var btnSubmit = $('button[type=submit]', form);
          var txtSubmit = btnSubmit.html();
          btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
          btnSubmit.attr('disabled', true);

          $(form).ajaxSubmit({
            dataType: 'json',
            type : 'post',
            success: function(res) {
              if(res.error != 0) {
                toastr.error(res.error);
              } else {
                toastr.success(res.success);
                $('#modal-add').modal('hide');
                setTimeout(function(){
                  location.href = res.redirect?res.redirect:'<?=site_url('site/user/dashboard')?>';
                }, 2000);
              }
            },
            error: function() {
              toastr.error('SERVER ERROR');
            },
            complete: function() {
              btnSubmit.html(txtSubmit);
              btnSubmit.attr('disabled', false);
            }
          });
          return false;
        }
      });
    });
    return false;
  });
});
</script>
