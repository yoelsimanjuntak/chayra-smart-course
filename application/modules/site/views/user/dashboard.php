<?php
$ruser = GetLoggedUser();
$rpackage = $this->db
->where(COL_PKGISACTIVE, 1)
->order_by(COL_PKGNAME)
->get(TBL_MTESTPACKAGE)
->result_array();

$rsess = array();
$rsess_ = array();
$rsessAll = array();
if($ruser[COL_ROLEID]==ROLEUSER) {
  $rsess = $this->db
  ->select('tsession.*, mtestpackage.PkgName, mtestpackage.PkgDesc, mtestpackage.PkgPrice, mtestpackage.PkgItems')
  ->join(TBL_MTESTPACKAGE,TBL_MTESTPACKAGE.'.'.COL_UNIQ." = ".TBL_TSESSION.".".COL_IDPACKAGE,"left")
  ->where("SessTimeEnd is null")
  ->where(COL_USERNAME, $ruser[COL_USERNAME])
  ->order_by('tsession.Uniq', 'desc')
  ->get(TBL_TSESSION)
  ->row_array();
  $rsess_ = $this->db
  ->select('tsession.*, mtestpackage.PkgName, mtestpackage.PkgDesc, mtestpackage.PkgPrice, mtestpackage.PkgItems')
  ->join(TBL_MTESTPACKAGE,TBL_MTESTPACKAGE.'.'.COL_UNIQ." = ".TBL_TSESSION.".".COL_IDPACKAGE,"left")
  ->where("SessTimeEnd is null")
  ->where(COL_USERNAME, $ruser[COL_USERNAME])
  ->order_by('tsession.Uniq', 'desc')
  ->get(TBL_TSESSION)
  ->result_array();

  $rsessAll = $this->db
  ->select('tsession.*, mtestpackage.PkgName, mtestpackage.PkgDesc, mtestpackage.PkgPrice, mtestpackage.PkgItems, (select sum(TestScore) from tsessiontest ts where ts.IDSession=tsession.Uniq) as SessScore')
  ->join(TBL_MTESTPACKAGE,TBL_MTESTPACKAGE.'.'.COL_UNIQ." = ".TBL_TSESSION.".".COL_IDPACKAGE,"left")
  ->where("SessTimeEnd is not null")
  ->where(COL_USERNAME, $ruser[COL_USERNAME])
  ->order_by(COL_SESSTIMEEND, 'desc')
  ->get(TBL_TSESSION)
  ->result_array();
}

$lastScore = '-';
if(!empty($rsessAll)&&count($rsessAll)>0) {
  $rtest = $this->db
  ->query("select * from tsessiontest where TestRemarks1 is null and IDSession=".$rsessAll[0][COL_UNIQ])
  ->result_array();

  $sum=0;
  foreach($rtest as $t) {
    $rquest = $this->db
    ->query("select sum(QuestScore) as QuestScore from tsessionsheet where IDTest=".$t[COL_UNIQ])
    ->row_array();

    if($t[COL_TESTNAME]!='Pass Hand A4') {
      $sum += isset($t[COL_TESTSCORE])?$t[COL_TESTSCORE]:$rquest[COL_QUESTSCORE];
    }
  }
  $lastScore = $sum;
}
?>
 <style>
 th {
   border-right-width: 1px !important;
 }
 .table thead tr:first-child td {
   border-bottom: none !important;
}
 </style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <?php
      if($ruser[COL_ROLEID]==ROLEADMIN) {
        $countUser = $this->db
        ->where(COL_ROLEID, ROLEUSER)
        ->count_all_results(TBL_USERS);

        $countUserActive = $this->db
        ->where(COL_ISSUSPEND, 0)
        ->where(COL_ROLEID, ROLEUSER)
        ->count_all_results(TBL_USERS);

        $countUserInactive = $this->db
        ->where(COL_ISSUSPEND, 1)
        ->where(COL_ROLEID, ROLEUSER)
        ->count_all_results(TBL_USERS);

        $countSess = $this->db
        ->count_all_results(TBL_TSESSION);

        $countSessNew = $this->db
        ->where(COL_SESSTIMEEND, null)
        ->count_all_results(TBL_TSESSION);

        $countSessEnd = $this->db
        ->where(COL_SESSTIMEEND.' != ', null)
        ->count_all_results(TBL_TSESSION);

        $countSubsActive = $this->db
        ->where(COL_SUBSDATETO.' >= ', date('Y-m-d'))
        ->count_all_results(TBL_TSUBSCRIPTION);

        $countSubsExpired = $this->db
        ->where(COL_SUBSDATETO.' < ', date('Y-m-d'))
        ->count_all_results(TBL_TSUBSCRIPTION);

        $countModActive = $this->db
        ->where(COL_MODISAKTIF, 1)
        ->count_all_results(TBL_MMODUL);

        $countModSuspended = $this->db
        ->where(COL_MODISAKTIF." != ", 1)
        ->count_all_results(TBL_MMODUL);
        ?>
        <div class="col-lg-3 col-12">
          <div class="small-box bg-info">
            <div class="inner">
              <h3><?=number_format($countUser)?></h3>
              <p class="mb-0">
                <strong>Pengguna</strong><br />
                <small>
                  <i class="far fa-check-circle"></i>&nbsp;AKTIF : <strong><?=number_format($countUser)?></strong><br />
                  <i class="far fa-times-circle"></i>&nbsp;SUSPENDED : <strong><?=number_format($countUserInactive)?></strong>
                </small>
              </p>
            </div>
            <div class="icon">
              <i class="fas fa-users"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-12">
          <div class="small-box bg-indigo">
            <div class="inner">
              <h3><?=number_format($countSubsActive+$countSubsExpired)?></h3>
              <p class="mb-0">
                <strong>Subscription</strong><br />
                <small>
                  <i class="far fa-check-circle"></i>&nbsp;AKTIF : <strong><?=number_format($countSubsActive)?></strong><br />
                  <i class="far fa-times-circle"></i>&nbsp;BERAKHIR : <strong><?=number_format($countSubsExpired)?></strong>
                </small>
              </p>
            </div>
            <div class="icon">
              <i class="fas fa-book-reader"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-12">
          <div class="small-box bg-primary">
            <div class="inner">
              <h3><?=number_format($countSess)?></h3>
              <p class="mb-0">
                <strong>Sesi Test / Ujian</strong><br />
                <small>
                  <i class="far fa-exclamation-circle"></i>&nbsp;BARU / BERJALAN : <strong><?=number_format($countSessNew)?></strong><br />
                  <i class="far fa-check-circle"></i>&nbsp;SELESAI : <strong><?=number_format($countSessEnd)?></strong>
                </small>
              </p>
            </div>
            <div class="icon">
              <i class="fas fa-file-edit"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-12">
          <div class="small-box bg-maroon">
            <div class="inner">
              <h3><?=number_format($countModActive+$countModSuspended)?></h3>
              <p class="mb-0">
                <strong>Modul Belajar</strong><br />
                <small>
                  <i class="far fa-check-circle"></i>&nbsp;DITERBITKAN : <strong><?=number_format($countModActive)?></strong><br />
                  <i class="far fa-exclamation-circle"></i>&nbsp;DITANGGUHKAN : <strong><?=number_format($countModSuspended)?></strong>
                </small>
              </p>
            </div>
            <div class="icon">
              <i class="fas fa-books"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-12 col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title font-weight-bold">SESI TEST / UJIAN BERJALAN</h3>
            </div>
            <div class="card-body table-responsive">
              <form id="dataform" method="post" action="#">
                <table id="datalist" class="table table-bordered table-hover table-condensed">
                  <thead>
                    <tr>
                      <th class="text-center" style="width: 10px">#</th>
                      <th>PAKET</th>
                      <th>PESERTA</th>
                      <th>MULAI</th>
                      <th>TEST BERJALAN</th>
                      <th>SISA WAKTU</th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
              </form>
            </div>
            <div class="card-footer text-right">
              <a href="<?=site_url('site/sess/index/active')?>" class="btn btn-sm btn-primary">LIHAT SEMUA&nbsp;<i class="far fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </div>
        <?php
      } else if($ruser[COL_ROLEID]==ROLESTAFF) {
        $rtest = $this->db
        ->where(COL_ISDELETED, 0)
        ->where(COL_TESTTYPE.' != ', 'ACR')
        ->where(COL_TESTTYPE.' != ', 'PAULI')
        ->order_by(COL_TESTNAME, 'asc')
        ->get(TBL_MTEST)
        ->result_array();
        ?>
        <div class="col-sm-12">
          <div class="form-group">
            <div class="input-group mb-3">
              <input type="text" name="filter" class="form-control" placeholder="Pencarian...">
              <div class="input-group-append">
                <span class="input-group-text"><i class="far fa-search"></i></span>
                <a href="<?=site_url('site/master/test-add')?>" class="btn btn-primary btn-form-test" data-title="Tambah"><i class="far fa-plus-circle"></i> TAMBAH</a>
              </div>
            </div>
          </div>
        </div>
        <?php
        if(!empty($rtest)) {
          foreach($rtest as $dat) {
            $numDetail = $this->db->where(COL_IDTEST, $dat[COL_UNIQ])->get(TBL_MTESTDETAIL)->num_rows();
            ?>
            <div class="col-md-4 card-data" data-name="<?=strtolower($dat[COL_TESTNAME])?>">
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title font-weight-bold"><?=$dat[COL_TESTNAME]?></h3>
                </div>
                <div class="card-body p-0">
                    <table class="table table-striped">
                    <tr>
                      <td style="width: 10px !important; white-space: nowrap">TIPE</td>
                      <td style="width: 10px !important; white-space: nowrap">:</td>
                      <td>
                        <strong><?=$dat[COL_TESTTYPE]=='MUL'?'UMUM':$dat[COL_TESTTYPE]?></strong><?=!empty($dat[COL_TESTREMARKS1])?' / <strong>'.$dat[COL_TESTREMARKS1].'</strong>':''?>
                        <?php
                        if($dat[COL_TESTTYPE]=='MUL' && !empty($dat[COL_TESTOPTION])) {
                          $arrOpts = json_decode($dat[COL_TESTOPTION]);
                          $arrOpts_ = array();
                          foreach($arrOpts as $opt) {
                            $arrOpts_[]=$opt->Opt;
                          }
                          if(!empty($arrOpts_)) {
                            echo '<small>('.implode(",", $arrOpts_).')</small>';
                          }
                        }
                        ?>
                      </td>
                    </tr>
                    <tr>
                      <td style="width: 10px !important; white-space: nowrap">JLH. SOAL / DURASI</td>
                      <td style="width: 10px !important; white-space: nowrap">:</td>
                      <td><strong><?=number_format($dat[COL_TESTQUESTNUM]*(!empty($dat[COL_TESTQUESTSUB])?$dat[COL_TESTQUESTSUB]:1))?></strong> / <strong><?=number_format($dat[COL_TESTDURATION]*(!empty($dat[COL_TESTQUESTSUB])?$dat[COL_TESTQUESTNUM]:1),2)?></strong> <small>(MENIT)</small></td>
                    </tr>
                    <tr>
                      <td style="width: 10px !important; white-space: nowrap">JLH. SOAL TERSEDIA</td>
                      <td style="width: 10px !important; white-space: nowrap">:</td>
                      <td><strong><?=number_format($numDetail)?></strong></td>
                    </tr>
                  </table>
                </div>
                <div class="card-footer">
                  <!--<a href="<?=site_url('site/master/question-add/'.$dat[COL_UNIQ])?>" class="btn btn-block btn-sm btn-primary btn-add-data" data-id="<?=$dat[COL_UNIQ]?>"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH SOAL</a>-->
                  <div class="row">
                    <div class="col-sm-4">
                      <a href="<?=site_url('site/master/test-edit/'.$dat[COL_UNIQ])?>" class="btn btn-block btn-sm btn-success btn-form-test" data-title="Ubah"><i class="far fa-cog"></i>&nbsp;UBAH</a>
                    </div>
                    <div class="col-sm-4">
                      <a href="<?=site_url('site/master/test-delete/'.$dat[COL_UNIQ])?>" class="btn btn-block btn-sm btn-danger btn-delete" data-prompt="Apakah anda yakin ingin menghapus?"><i class="far fa-trash"></i>&nbsp;HAPUS</a>
                    </div>
                    <div class="col-sm-4">
                      <a href="<?=site_url('site/master/question/'.$dat[COL_UNIQ])?>" class="btn btn-block btn-sm btn-primary"><i class="far fa-list"></i>&nbsp;KELOLA SOAL</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php
          }
        } else {
          ?>
          <div class="col-md-12">
            <div class="card">
              <div class="card-body">
                <p class="text-center mb-0 font-italic">
                  BELUM ADA MODEL SOAL TERSEDIA
                </p>
              </div>
            </div>
          </div>
          <?php
        }
      }
      ?>
    </div>
  </div>
</section>
<div id="dom-filter" class="d-none">
  <select class="form-control" name="filterPackage" style="width: 200px">
    <?=GetCombobox("select * from mtestpackage order by PkgName", COL_UNIQ, COL_PKGNAME, null, true, false, '-- SEMUA PAKET --')?>
  </select>
</div>
<div class="modal fade" id="modal-test" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">PAKET SIMULASI</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
          <?php
          if(empty($rpackage)) {
            ?>
            <p class="text-muted">Maaf, belum ada paket tersedia untuk saat ini.</p>
            <?php
          } else {
            ?>
            <p class="text-muted font-italic">Silakan pilih paket simulasi dibawah ini:</p>
            <?php
            foreach($rpackage as $p) {
              $txt = urlencode("Saya ingin mendapatkan akses simulasi *".strtoupper($p[COL_PKGNAME])."* di ".$this->setting_web_desc."\n\nBerikut info akun saya:\nNama: *".$ruser[COL_FULLNAME]."*\nUsername: *".$ruser[COL_USERNAME]."*");
              ?>
              <div class="callout callout-info">
                <h6 class="font-weight-bold"><?=strtoupper($p[COL_PKGNAME])?><span class="badge badge-success float-right">Rp. <?=number_format($p[COL_PKGPRICE])?></span></h6>
                <p class="text-sm text-muted mb-2"><?=$p[COL_PKGDESC]?></p>
                <p>
                  <a href="https://api.whatsapp.com/send?phone=<?=$this->setting_org_phone?>&text=<?=$txt?>" target="_blank" class="btn btn-sm btn-info" style="color: #fff; text-decoration: none">PILIH&nbsp;<i class="far fa-arrow-circle-right"></i></a>
                </p>
              </div>
              <?php
            }
          }
          ?>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-start" role="dialog">
  <div class="modal-dialog modal-sm modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">MULAI SESI</h5>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer d-none">
        <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;TUTUP</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-add" role="dialog">
  <div class="modal-dialog modal-sm modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">TAMBAH SOAL</span>
      </div>
      <form action="<?=site_url('site/master/question-add')?>" method="get">
        <div class="modal-body">
          <div class="form-group row mb-0">
            <label class="col-sm-4 control-label">JLH. SOAL</label>
            <div class="col-sm-8">
              <input type="hidden" name="<?=COL_IDTEST?>" value="" />
              <input type="number" class="form-control text-right" name="num" value="1" />
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
          <button type="submit" class="btn btn-sm btn-outline-success"><i class="far fa-arrow-circle-right"></i>&nbsp;LANJUT</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  <?php
  if($ruser[COL_ROLEID]==ROLEADMIN) {
    ?>
    var dt = $('#datalist').dataTable({
      "autoWidth" : false,
      "processing": true,
      "serverSide": true,
      "ajax": {
        "url": "<?=site_url('site/sess/index-load/active')?>",
        "type": 'POST',
        "data": function(data){
          data.filterPackage = $('[name=filterPackage]', $('.filtering')).val();
         }
      },
      "iDisplayLength": 100,
      "oLanguage": {
        "sSearch": "FILTER "
      },
      "dom":"R<'row'<'col-sm-12 d-flex'f<'filtering'>>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
      "order": [[ 3, "desc" ]],
      "columnDefs": [
        {"targets":[0], "className":'nowrap text-center'},
        {"targets":[3, 5], "className":'nowrap dt-body-right'}
      ],
      "columns": [
        {"orderable": false,"width": "50px"},
        {"orderable": true},
        {"orderable": true},
        {"orderable": true},
        {"orderable": true},
        {"orderable": false,"width": "10px"}
      ],
      "createdRow": function(row, data, dataIndex) {
        $('.btn-action', $(row)).click(function() {
          var url = $(this).attr('href');
          if(confirm('Apakah anda yakin?')) {
            $.get(url, function(res) {
              if(res.error != 0) {
                toastr.error(res.error);
              } else {
                toastr.success(res.success);
              }
            }, "json").done(function() {
              dt.DataTable().ajax.reload();
            }).fail(function() {
              toastr.error('SERVER ERROR');
            });
          }
          return false;
        });
        $('[data-toggle="tooltip"]', $(row)).tooltip();
      },
      "initComplete": function(settings, json) {
        $('input[type=search]', $('#datalist_filter')).removeClass('form-control-sm').attr('placeholder', 'Keyword');
      }
    });
    $("div.filtering").html($('#dom-filter').html()).addClass('d-inline-block ml-2');
    $('.btn-refresh-data').click(function() {
      dt.DataTable().ajax.reload();
    });
    $('input,select', $("div.filtering")).change(function() {
      dt.DataTable().ajax.reload();
    });
    setInterval(function(){
      dt.DataTable().ajax.reload();
    }, 5000);
    <?php
  }
  ?>
  $('#btn-profil').click(function(){
    $('#btn-changeprofile').click();
  });

  $('[name=filter]').keyup(function(){
    var key = $('[name=filter]').val();
    if(key) {
      $('.card-data').hide();
      $('.card-data[data-name*="'+key.toLowerCase()+'"]').show();
    } else {
      $('.card-data').show();
    }
  });

  $('.btn-add-data').click(function() {
    var idTest = $(this).data('id');

    if(idTest) {
      $('[name=IDTest]', $('#modal-add')).val(idTest);
    }
    $('#modal-add').modal('show');
    return false;
  });

  $('.btn-form-test').click(function() {
    var url = $(this).attr('href');
    var title = $(this).data('title');
    if(url) {
      if(title) {
        $('.modal-title', $('#modal-form')).html(title);
      }

      $('#modal-form').modal('show');
      $('.modal-body', $('#modal-form')).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
      $('.modal-body', $('#modal-form')).load(url, function(){
        $('button[type=submit]', $('#modal-form')).unbind('click').click(function(){
          $('form', $('#modal-form')).submit();
        });
      });
    }

    return false;
  });

  $('.btn-delete').click(function() {
    var url = $(this).attr('href');
    var prompt = $(this).data('prompt');
    if(confirm((prompt||'Apakah anda yakin?'))) {
      $.get(url, function(res) {
        if(res.error != 0) {
          toastr.error(res.error);
        } else {
          toastr.success(res.success);
        }
      }, "json").done(function() {
        location.reload();
      }).fail(function() {
        toastr.error('SERVER ERROR');
      });
    }

    return false;
  });
});
</script>
