<?php
$ruser = GetLoggedUser();
$rpackage = $this->db
->where(COL_PKGISACTIVE, 1)
->order_by(COL_PKGNAME)
->get(TBL_MTESTPACKAGE)
->result_array();

$rsess = array();
$rsess_ = array();
$rsessAll = array();
$rsess = $this->db
->select('tsession.*, mtestpackage.PkgName, mtestpackage.PkgDesc, mtestpackage.PkgPrice, mtestpackage.PkgItems')
->join(TBL_MTESTPACKAGE,TBL_MTESTPACKAGE.'.'.COL_UNIQ." = ".TBL_TSESSION.".".COL_IDPACKAGE,"left")
->where("SessTimeEnd is null")
->where(COL_USERNAME, $ruser[COL_USERNAME])
->order_by('tsession.Uniq', 'desc')
->get(TBL_TSESSION)
->row_array();
$rsess_ = $this->db
->select('tsession.*, mtestpackage.PkgName, mtestpackage.PkgDesc, mtestpackage.PkgPrice, mtestpackage.PkgItems')
->join(TBL_MTESTPACKAGE,TBL_MTESTPACKAGE.'.'.COL_UNIQ." = ".TBL_TSESSION.".".COL_IDPACKAGE,"left")
->where("SessTimeEnd is null")
->where(COL_USERNAME, $ruser[COL_USERNAME])
->order_by('tsession.Uniq', 'desc')
->get(TBL_TSESSION)
->result_array();

$rmodul = $this->db
->where(COL_MODISAKTIF, 1)
->order_by(COL_CREATEDON, 'desc')
->get(TBL_MMODUL)
->result_array();

$rsubs = $this->db
->select('tsubscription.*, mkategori.Kategori')
->join(TBL_MKATEGORI,TBL_MKATEGORI.'.'.COL_UNIQ." = ".TBL_TSUBSCRIPTION.".".COL_IDKATEGORI,"left")
->where(COL_SUBSDATETO.' >= ', date('Y-m-d'))
->where(COL_USERNAME, $ruser[COL_USERNAME])
->get(TBL_TSUBSCRIPTION)
->result_array();
?>
 <style>
 th {
   border-right-width: 1px !important;
 }
 .table thead tr:first-child td {
   border-bottom: none !important;
}
 </style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <?php
      if(empty($ruser[COL_GENDER]) || empty($ruser[COL_PHONE]) || empty($ruser[COL_DATEBIRTH])) {
        ?>
        <div class="col-lg-12">
          <div class="callout callout-danger">
            <h6 class="font-weight-bold"><i class="fas fa-exclamation-circle text-danger"></i>&nbsp;PROFIL BELUM LENGKAP !</h6>
            <p class="font-italic">
              <strong><?=$ruser[COL_FULLNAME]?></strong>, profil kamu belum lengkap, nih.<br />
              Silakan lengkapi dan perbarui profil kamu sebelum kamu menikmati fitur dan layanan <?=$this->setting_web_name?> ya!
            </p>
            <p>
              <button type="button" id="btn-profil" class="btn btn-sm btn-danger">PERBARUI PROFIL&nbsp;<i class="far fa-arrow-circle-right"></i></button>
            </p>
          </div>
        </div>
        <?php
      }
      ?>
      <?php
      if(!empty($rsess) && !empty($rsess[COL_SESSTIMESTART])) {
        $rpackageCurr = $this->db
        ->where(COL_UNIQ, $rsess[COL_IDPACKAGE])
        ->get(TBL_MTESTPACKAGE)
        ->row_array();
        if(!empty($rpackageCurr)) {
          $arrPkgItems = json_decode($rpackageCurr[COL_PKGITEMS]);
          ?>
          <div class="col-lg-6">
            <div class="card card-outline card-indigo">
              <div class="card-header">
                <h6 class="card-title font-weight-bold"><i class="far fa-hourglass"></i>&nbsp;&nbsp;LANJUTKAN SESI</h6>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <p class="mb-0">
                  Hai, <strong><?=strtoupper($ruser[COL_FULLNAME])?></strong>!<br />
                  Kamu memiliki sesi <strong style="text-decoration: underline"><?=strtoupper($rsess[COL_PKGNAME])?></strong> yang sedang berjalan. Silakan klik tombol dibawah untuk melanjutkan sesi sebelum waktu habis.
                </p>
              </div>
              <div class="card-footer">
                <p class="text-right mb-0">
                  <a <?=empty($rsess[COL_SESSTIMESTART])?'id="btn-start"':''?> href="<?=site_url('site/sess/start/'.$rsess[COL_UNIQ])?>" class="btn btn-sm btn-success"><?=!empty($rsess[COL_SESSTIMESTART])?'LANJUTKAN':'MULAI'?>&nbsp;&nbsp;<i class="far fa-arrow-circle-right"></i></a>
                </p>
              </div>
            </div>
          </div>
          <?php
        }
      } else {
        ?>
        <div class="col-lg-6">
          <div class="card card-outline card-indigo">
            <div class="card-header">
              <h6 class="card-title font-weight-bold"><i class="far fa-edit"></i>&nbsp;&nbsp;SESI SIMULASI</h6>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body p-0">
              <?php
              if(!empty($rsess_)) {
                ?>
                <p class="p-3 mb-0">
                  Hai, <strong><?=strtoupper($ruser[COL_FULLNAME])?></strong>!<br />
                  Kamu memiliki sesi simulasi yang belum dikerjakan. Silakan klik tombol MULAI untuk mengerjakan sesi berikut.
                </p>
                <ul class="nav nav-pills flex-column pl-1 pr-1">
                  <?php
                  foreach($rsess_ as $r) {
                    ?>
                    <li class="nav-item" style="border-top: 1px solid #dedede">
                      <span class="nav-link" style="padding: 1rem 1rem !important">
                        <strong class="text-indigo"><?=$r[COL_PKGNAME]?></strong>
                        <span class="float-right text-danger">
                          <a class="btn btn-sm btn-success btn-start" href="<?=site_url('site/sess/start/'.$r[COL_UNIQ])?>">MULAI&nbsp;&nbsp;<i class="far fa-arrow-circle-right"></i></a>
                        </span>
                      </span>
                    </li>
                    <?php
                  }
                  ?>
                </ul>
                <?php
              } else {
                ?>
                <p class="p-3 mb-0 font-italic">
                  Hai, <strong><?=$ruser[COL_FULLNAME]?></strong> kamu belum memiliki sesi simulasi / ujian yang tersedia. Kamu bisa mengikuti simulasi melalui link dibawah ini.
                </p>
                <?php
              }
              ?>
            </div>
            <div class="card-footer text-right">
              <a href="<?=site_url('site/user/sess-history')?>" class="btn btn-sm btn-success"><i class="far fa-history"></i>&nbsp;LIHAT RIWAYAT</i></a>
              <a href="<?=site_url('site/user/package')?>" class="btn btn-sm btn-primary">TAMBAH SESI SIMULASI&nbsp;<i class="far fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </div>
        <?php
      }
      ?>
      <div class="col-lg-6">
        <div class="card card-outline card-indigo">
          <div class="card-header">
            <h6 class="card-title font-weight-bold"><i class="far fa-book-reader"></i>&nbsp;&nbsp;PAKET BERLANGGANAN MODUL</h6>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body p-0">
            <?php
            if(!empty($rsubs)) {
              ?>
              <table class="table">
                <tbody>
                  <?php
                  foreach($rsubs as $sub) {
                    $subRemain = round((strtotime($sub[COL_SUBSDATETO]) - time()) / (60 * 60 * 24));
                    ?>
                    <tr>
                      <td><?=$sub[COL_KATEGORI]?></td>
                      <td class="text-right"><strong><?=number_format($subRemain>=0?$subRemain:0)?></strong> <small class="text-sm">HARI</small></td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
              <?php
            } else {
              ?>
              <p class="p-3 mb-0 font-italic">
                Hai, <strong><?=$ruser[COL_FULLNAME]?></strong> kamu belum memiliki paket berlangganan modul belajar yang aktif. Kamu bisa mengaktifkan paket melalui link dibawah ini.
              </p>
              <?php
            }
            ?>
          </div>
          <div class="card-footer text-center">
            <a href="<?=site_url('site/user/subscription')?>" class="btn btn-sm btn-primary"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH PAKET</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div id="dom-filter" class="d-none">
  <select class="form-control" name="filterPackage" style="width: 200px">
    <?=GetCombobox("select * from mtestpackage order by PkgName", COL_UNIQ, COL_PKGNAME, null, true, false, '-- SEMUA PAKET --')?>
  </select>
</div>
<div class="modal fade" id="modal-test" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">PAKET SIMULASI</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
          <?php
          if(empty($rpackage)) {
            ?>
            <p class="text-muted">Maaf, belum ada paket tersedia untuk saat ini.</p>
            <?php
          } else {
            ?>
            <p class="text-muted font-italic">Silakan pilih paket simulasi dibawah ini:</p>
            <?php
            foreach($rpackage as $p) {
              $txt = urlencode("Saya ingin mendapatkan akses simulasi *".strtoupper($p[COL_PKGNAME])."* di ".$this->setting_web_desc."\n\nBerikut info akun saya:\nNama: *".$ruser[COL_FULLNAME]."*\nUsername: *".$ruser[COL_USERNAME]."*");
              ?>
              <div class="callout callout-info">
                <h6 class="font-weight-bold"><?=strtoupper($p[COL_PKGNAME])?><span class="badge badge-success float-right">Rp. <?=number_format($p[COL_PKGPRICE])?></span></h6>
                <p class="text-sm text-muted mb-2"><?=$p[COL_PKGDESC]?></p>
                <p>
                  <a href="https://api.whatsapp.com/send?phone=<?=$this->setting_org_phone?>&text=<?=$txt?>" target="_blank" class="btn btn-sm btn-info" style="color: #fff; text-decoration: none">PILIH&nbsp;<i class="far fa-arrow-circle-right"></i></a>
                </p>
              </div>
              <?php
            }
          }
          ?>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-start" role="dialog">
  <div class="modal-dialog modal-sm modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">MULAI SESI</h5>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer d-none">
        <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;TUTUP</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  <?php
  if($ruser[COL_ROLEID]==ROLEADMIN) {
    ?>
    var dt = $('#datalist').dataTable({
      "autoWidth" : false,
      "processing": true,
      "serverSide": true,
      "ajax": {
        "url": "<?=site_url('site/sess/index-load/active')?>",
        "type": 'POST',
        "data": function(data){
          data.filterPackage = $('[name=filterPackage]', $('.filtering')).val();
         }
      },
      "iDisplayLength": 100,
      "oLanguage": {
        "sSearch": "FILTER "
      },
      "dom":"R<'row'<'col-sm-12 d-flex'f<'filtering'>>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
      "order": [[ 3, "desc" ]],
      "columnDefs": [
        {"targets":[0], "className":'nowrap text-center'},
        {"targets":[3, 5], "className":'nowrap dt-body-right'}
      ],
      "columns": [
        {"orderable": false,"width": "50px"},
        {"orderable": true},
        {"orderable": true},
        {"orderable": true},
        {"orderable": true},
        {"orderable": false,"width": "10px"}
      ],
      "createdRow": function(row, data, dataIndex) {
        $('.btn-action', $(row)).click(function() {
          var url = $(this).attr('href');
          if(confirm('Apakah anda yakin?')) {
            $.get(url, function(res) {
              if(res.error != 0) {
                toastr.error(res.error);
              } else {
                toastr.success(res.success);
              }
            }, "json").done(function() {
              dt.DataTable().ajax.reload();
            }).fail(function() {
              toastr.error('SERVER ERROR');
            });
          }
          return false;
        });
        $('[data-toggle="tooltip"]', $(row)).tooltip();
      },
      "initComplete": function(settings, json) {
        $('input[type=search]', $('#datalist_filter')).removeClass('form-control-sm').attr('placeholder', 'Keyword');
      }
    });
    $("div.filtering").html($('#dom-filter').html()).addClass('d-inline-block ml-2');
    $('.btn-refresh-data').click(function() {
      dt.DataTable().ajax.reload();
    });
    $('input,select', $("div.filtering")).change(function() {
      dt.DataTable().ajax.reload();
    });
    setInterval(function(){
      dt.DataTable().ajax.reload();
    }, 5000);
    <?php
  }
  ?>
  $('#btn-profil').click(function(){
    $('#btn-changeprofile').click();
  });
  $('.btn-start').click(function() {
    var href = $(this).attr('href');
    $('.modal-body', $('#modal-start')).html('<p class="text-center">MEMUAT DATA<br /><i class="far fa-spinner fa-spin"></i></p>');
    $('.modal-footer', $('#modal-start')).addClass('d-none');
    $('#modal-start').modal({backdrop: 'static', keyboard: false});
    $.get(href, function(data) {
      data=JSON.parse(data);

      if(data.error) {
        $('.modal-body', $('#modal-start')).html('<p class="text-danger mb-0">'+data.error+'</p>');
        $('.modal-footer', $('#modal-start')).removeClass('d-none');
        return false;
      }

      if(!data.redirect) {
        $('.modal-body', $('#modal-start')).html('<p class="text-danger mb-0">Mohon maaf, telah terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi.</p>');
        $('.modal-footer', $('#modal-start')).removeClass('d-none');
        return false;
      }

      $('.modal-body', $('#modal-start')).html('<p class="text-center">MENGALIHKAN<br /><i class="far fa-spinner fa-spin"></i></p>');
      setTimeout(function(){
        $('.modal-body', $('#modal-start')).html('');
        $('#modal-start').modal('hide');
        window.location.href = data.redirect;
      }, 3000);
    });
    return false;
  });
});
</script>
