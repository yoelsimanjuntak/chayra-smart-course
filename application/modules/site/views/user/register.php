
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?=!empty($title) ? $this->setting_web_name.' - '.$title : $this->setting_web_name?></title>
  <link rel="icon" type="image/png" href="<?=base_url().$this->setting_web_logo?>">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/mazer/assets/compiled/css/app.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/mazer/assets/compiled/css/app-dark.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/mazer/assets/compiled/css/auth.css">

  <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
  <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />

  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <script src="<?=base_url()?>assets/themes/mazer/assets/static/js/initTheme.js"></script>

  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.css">
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.js"></script>

  <!-- daterange picker -->
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">

  <style>
  .se-pre-con {
      position: fixed;
      left: 0px;
      top: 0px;
      width: 100%;
      height: 100%;
      z-index: 9999;
      background: url("<?=base_url().$this->setting_web_preloader?>") center no-repeat #fff;
  }
  @media screen and (max-width: 576px) {
    #auth #auth-left {
      padding: 5rem 1.5rem !important;
    }
  }
  </style>
  <script>
  $(window).load(function() {
    $(".se-pre-con").fadeOut("slow");
  });
  </script>
</head>

<body>
  <div class="se-pre-con"></div>
  <div id="auth">
    <div class="row h-100">
      <div class="col-lg-7 col-12">
        <div id="auth-left" class="py-5">
            <div class="mb-5 d-flex align-items-center">
              <div class="auth-logo m-0">
                <img src="<?=base_url().$this->setting_web_logo?>" alt="Logo" style="height: 4rem !important">
              </div>
              <div class="p-2" style="padding-left: 1.5rem !important">
                <h2 class="mb-0"><?=$this->setting_web_desc?></h2>
                <p class="mb-0">Pendaftaran Akun</p>
              </div>
            </div>
            <p class="mb-3" style="font-style: italic">Silakan isi form pendaftaran berikut ini.</p>
            <?= form_open(current_url(),array('id'=>'form-register')) ?>
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group position-relative has-icon-left mb-4">
                  <input type="text" class="form-control" name="<?=COL_FULLNAME?>" placeholder="Nama Lengkap" required />
                  <div class="form-control-icon">
                    <i class="fas fa-user"></i>
                  </div>
                </div>

                <div class="form-group position-relative has-icon-left mb-4">
                  <input type="email" class="form-control" name="<?=COL_EMAIL?>" placeholder="Email" required />
                  <div class="form-control-icon">
                    <i class="fas fa-at"></i>
                  </div>
                </div>

                <div class="form-group position-relative has-icon-left mb-4">
                  <input type="text" class="form-control" name="<?=COL_PHONE?>" placeholder="No. Telp / HP" required />
                  <div class="form-control-icon">
                    <i class="fas fa-mobile"></i>
                  </div>
                </div>

                <div class="form-group position-relative has-icon-left mb-4">
                  <input type="text" class="form-control" name="<?=COL_NMSOCIALMEDIA?>" placeholder="Akun Instagram" required />
                  <div class="form-control-icon">
                    <i class="fab fa-instagram"></i>
                  </div>
                </div>

                <div class="form-group position-relative has-icon-left mb-4">
                  <input type="text" class="form-control" name="<?=COL_NMSCHOOL?>" placeholder="Asal Sekolah" required />
                  <div class="form-control-icon">
                    <i class="far fa-building"></i>
                  </div>
                </div>

                <div class="form-group position-relative has-icon-left mb-4">
                  <input type="text" class="form-control" name="<?=COL_NMPROGRAM?>" placeholder="Program Belajar" required />
                  <div class="form-control-icon">
                    <i class="far fa-graduation-cap"></i>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group position-relative has-icon-left mb-4">
                  <input type="text" class="form-control" name="<?=COL_NMORANGTUA1?>" placeholder="Nama Ayah" required />
                  <div class="form-control-icon">
                    <i class="far fa-user"></i>
                  </div>
                </div>

                <div class="form-group position-relative has-icon-left mb-4">
                  <input type="text" class="form-control" name="<?=COL_NMORANGTUA2?>" placeholder="Nama Ibu" required />
                  <div class="form-control-icon">
                    <i class="far fa-user"></i>
                  </div>
                </div>

                <div class="form-group position-relative has-icon-left mb-4">
                  <input type="text" class="form-control" name="<?=COL_NMPEKERJAANORTU1?>" placeholder="Pekerjaan Ayah" required />
                  <div class="form-control-icon">
                    <i class="far fa-user"></i>
                  </div>
                </div>

                <div class="form-group position-relative has-icon-left mb-4">
                  <input type="text" class="form-control" name="<?=COL_NMPEKERJAANORTU2?>" placeholder="Pekerjaan Ibu" required />
                  <div class="form-control-icon">
                    <i class="far fa-briefcase"></i>
                  </div>
                </div>

                <div class="form-group position-relative has-icon-left mb-4">
                  <input type="text" class="form-control" name="<?=COL_NOHPORTU1?>" placeholder="No. HP Ayah" required />
                  <div class="form-control-icon">
                    <i class="far fa-mobile"></i>
                  </div>
                </div>

                <div class="form-group position-relative has-icon-left mb-4">
                  <input type="text" class="form-control" name="<?=COL_NOHPORTU2?>" placeholder="No. HP Ibu" required />
                  <div class="form-control-icon">
                    <i class="far fa-mobile"></i>
                  </div>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group position-relative has-icon-left mb-4">
                  <label for="userfile" class="form-label">Avatar / Foto Profil</label>
                  <input type="file" name="userfile" class="form-control" accept="image/*" style="padding: .375rem .75rem !important">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group position-relative has-icon-left mb-4">
                  <input type="password" class="form-control" name="<?=COL_PASSWORD?>" placeholder="Password" required />
                  <div class="form-control-icon">
                    <i class="far fa-lock"></i>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group position-relative has-icon-left mb-4">
                  <input type="password" class="form-control" name="ConfirmPassword" placeholder="Konfirmasi Password" required />
                  <div class="form-control-icon">
                    <i class="far fa-lock"></i>
                  </div>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary btn-block shadow-lg mt-3">Daftar <i class="fas fa-sign-in"></i></button>
            <?= form_close(); ?>

            <div class="mt-5">
                <p class="text-gray-600">
                  Sudah memiliki akun? silakan <a href="<?=site_url('site/user/login')?>" class="font-bold">Login</a>.
                </p>
            </div>
        </div>
      </div>
      <div class="col-lg-5 d-none d-lg-block">
        <div id="auth-right" style="background-size: cover !important; background:url('<?=MY_IMAGEURL.'bg-login.png'?>'),linear-gradient(90deg,#8261ee,#8261ee)">
        </div>
      </div>
    </div>
  </div>
  <script src="<?=base_url()?>assets/js/jquery.particleground.js"></script>
  <!--<script type="text/javascript" src="<?=base_url() ?>assets/js/jquery.blockUI.js"></script>-->
  <script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.validate.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/template/js/function.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.form.js"></script>

  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
  <script>
  $(document).ready(function() {
    bsCustomFileInput.init();
    $('#auth-right').particleground({
      dotColor: '#ff589e',
      lineColor: '#fff'
    });
    $('#intro').css({
      'margin-top': -($('#intro').height() / 2)
    });

    $('#form-register').validate({
      ignore: "input[type='file']",
      submitHandler: function(form) {
        var btnSubmit = $('button[type=submit]', $(form));
        var txtSubmit = btnSubmit[0].innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>').attr('disabled', true);
        $(form).ajaxSubmit({
          dataType: 'json',
          type : 'post',
          success: function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
              btnSubmit.html(txtSubmit).attr('disabled', false);
            } else {
              toastr.success(res.success);
              if(res.redirect) {
                setTimeout(function(){
                  location.href = res.redirect;
                }, 1000);
              }
            }
          },
          error: function() {
            toastr.error('SERVER ERROR');
            btnSubmit.html(txtSubmit).attr('disabled', false);
          },
          complete: function() {
            //btnSubmit.html(txtSubmit).attr('disabled', false);
          }
        });

        return false;
      }
    });
  });
  </script>
</body>
</html>
