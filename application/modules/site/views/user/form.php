<form id="form-user" action="<?=current_url()?>" method="post">
  <div class="row">
    <div class="col-sm-12">
      <div class="form-group">
        <label>Nama Lengkap</label>
        <input type="text" name="<?=COL_FULLNAME?>" class="form-control" value="<?=!empty($data)?$data[COL_FULLNAME]:''?>" required />
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label>Email</label>
        <input type="email" name="<?=COL_EMAIL?>" class="form-control" value="<?=!empty($data)?$data[COL_EMAIL]:''?>" required />
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label>No. Telp</label>
        <input type="text" name="<?=COL_PHONE?>" class="form-control" value="<?=!empty($data)?$data[COL_PHONE]:''?>" required />
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label>Jenis Kelamin</label>
        <select class="form-control" name="<?=COL_GENDER?>" style="width: 100%" required>
          <option value="MALE" <?=!empty($data)&&$data[COL_GENDER]=='MALE'?'selected':''?> >LAKI-LAKI</option>
          <option value="FEMALE" <?=!empty($data)&&$data[COL_GENDER]=='FEMALE'?'selected':''?>>PEREMPUAN</option>
        </select>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label>Tgl. Lahir</label>
        <input type="text" class="form-control datepicker text-right" name="<?=COL_DATEBIRTH?>" value="<?=!empty($data)?$data[COL_DATEBIRTH]:''?>" />
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label>No. HP / WA</label>
        <input type="text" class="form-control" name="<?=COL_PHONE?>" value="<?=!empty($data)?$data[COL_PHONE]:''?>" />
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label>Akun Instagram</label>
        <input type="text" class="form-control" name="<?=COL_NMSOCIALMEDIA?>" value="<?=!empty($data)?$data[COL_NMSOCIALMEDIA]:''?>" />
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label>Asal Sekolah</label>
        <input type="text" class="form-control" name="<?=COL_NMSCHOOL?>" value="<?=!empty($data)?$data[COL_NMSCHOOL]:''?>" />
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label>Program Belajar</label>
        <input type="text" class="form-control" name="<?=COL_NMPROGRAM?>" value="<?=!empty($data)?$data[COL_NMPROGRAM]:''?>" />
      </div>
    </div>
    <div class="col-sm-12">
      <div class="form-group">
        <label>Password <?=!empty($data)?'<small id="info-password" class="text-muted">(isi jika ingin mengubah password)</small>':''?></label>
        <input type="password" name="<?=COL_PASSWORD?>" class="form-control" />
      </div>
    </div>
  </div>
  <div class="row mt-3">
    <div class="col-sm-4 text-center">
      <img src="<?=!empty($data)&&!empty($data[COL_PROFILEPIC])?MY_UPLOADURL.$data[COL_PROFILEPIC]:MY_IMAGEURL.'icon-user.jpg'?>" class="profile-user-img img-fluid img-circle" style="border: 2px solid #adb5bd" alt="Avatar / Foto Profil">
    </div>
    <div class="col-sm-8">
      <label>Avatar / Foto Profil</label>
      <div class="input-group">
        <div class="custom-file">
          <input type="file" class="custom-file-input" name="userfile" accept="image/*">
          <label class="custom-file-label" for="userfile">Unggah Foto</label>
        </div>
      </div>
      <small class="text-muted font-italic">Ukuran maks. 2mB</small>
    </div>
  </div>
  <div class="form-group text-right mt-3">
    <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
    <button type="submit" class="btn btn-sm btn-outline-success"><i class="far fa-arrow-circle-right"></i>&nbsp;LANJUT</button>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function() {
  bsCustomFileInput.init();
  $('#form-user').validate({
    ignore: "input[type='file']",
    submitHandler: function(form) {
      var modal = $(form).closest('modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>
