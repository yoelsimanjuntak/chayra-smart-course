<?php
$htmlTitle = $this->setting_web_name;
$htmlLogo = base_url().$this->setting_web_logo;
?>
<html>
<head>
  <title><?=$this->setting_web_name.' - Biodata '.$rdata[COL_FULLNAME]?></title>
  <style>
  body {
    font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
  }
  th, td {
    padding: 5px;
  }
  table {
    width: 100%;
    border-collapse: collapse;
  }
  table, th, td {
    /*border: 1px solid black !important;*/
  }
  </style>
</head>
<body>
  <h4 style="padding-top: 15px !important; margin-bottom: 15px !important; text-align: center; text-decoration: underline">
    BIODATA SISWA
  </h4>
  <table width="100%" style="margin-top: 15px !important; margin-bottom: 15px !important">
    <?php
    if(!empty($rdata[COL_PROFILEPIC])&&file_exists(MY_UPLOADPATH.$rdata[COL_PROFILEPIC])) {
      ?>
      <tr>
        <td colspan="3" style="text-align: center; padding-bottom: 20px !important;">
          <img src="<?=MY_UPLOADURL.$rdata[COL_PROFILEPIC]?>" style="max-height: 200px !important; border: 0.25px solid #000; padding: 2px !important; padding-left: 2px !important; padding-right: 2px !important" />
        </td>
      </tr>
      <?php
    }
    ?>
    <tr>
      <td style="width: 10px; white-space: nowrap">NAMA LENGKAP</td>
      <td style="width: 10px">:</td>
      <td><strong><?=$rdata[COL_FULLNAME]?></strong></td>
    </tr>
    <!--<tr>
      <td style="width: 10px; white-space: nowrap">TEMPAT / TGL. LAHIR</td>
      <td style="width: 10px">:</td>
      <td><strong><?=$rdata[COL_BIRTH]?></strong></td>
    </tr>-->
    <tr>
      <td style="width: 10px; white-space: nowrap">ASAL SEKOLAH</td>
      <td style="width: 10px">:</td>
      <td><strong><?=$rdata[COL_NMSCHOOL]?></strong></td>
    </tr>
    <tr>
      <td style="width: 10px; white-space: nowrap">PROGRAM YANG DIPILIH</td>
      <td style="width: 10px">:</td>
      <td><strong><?=$rdata[COL_NMPROGRAM]?></strong></td>
    </tr>
    <tr>
      <td style="width: 10px; white-space: nowrap">NO. HP</td>
      <td style="width: 10px">:</td>
      <td><strong><?=$rdata[COL_PHONE]?></strong></td>
    </tr>
    <tr>
      <td style="width: 10px; white-space: nowrap">AKUN INSTAGRAM</td>
      <td style="width: 10px">:</td>
      <td><strong><?=$rdata[COL_NMSOCIALMEDIA]?></strong></td>
    </tr>
    <tr>
      <td style="width: 10px; white-space: nowrap">NAMA AYAH</td>
      <td style="width: 10px">:</td>
      <td><strong><?=$rdata[COL_NMORANGTUA1]?></strong></td>
    </tr>
    <tr>
      <td style="width: 10px; white-space: nowrap">NAMA IBU</td>
      <td style="width: 10px">:</td>
      <td><strong><?=$rdata[COL_NMORANGTUA2]?></strong></td>
    </tr>
    <tr>
      <td style="width: 10px; white-space: nowrap">PEKERJAAN AYAH</td>
      <td style="width: 10px">:</td>
      <td><strong><?=$rdata[COL_NMPEKERJAANORTU1]?></strong></td>
    </tr>
    <tr>
      <td style="width: 10px; white-space: nowrap">PEKERJAAN AYAH</td>
      <td style="width: 10px">:</td>
      <td><strong><?=$rdata[COL_NMPEKERJAANORTU2]?></strong></td>
    </tr>
    <tr>
      <td style="width: 10px; white-space: nowrap">NO. HP AYAH</td>
      <td style="width: 10px">:</td>
      <td><strong><?=$rdata[COL_NOHPORTU1]?></strong></td>
    </tr>
    <tr>
      <td style="width: 10px; white-space: nowrap">NO. HP IBU</td>
      <td style="width: 10px">:</td>
      <td><strong><?=$rdata[COL_NOHPORTU2]?></strong></td>
    </tr>
    <tr>
      <td style="width: 10px; white-space: nowrap">WAKTU PENDAFTARAN</td>
      <td style="width: 10px">:</td>
      <td><strong><?=date('d-m-Y H:i', strtotime($rdata[COL_CREATEDON]))?></strong></td>
    </tr>
  </table>
</body>
</html>
