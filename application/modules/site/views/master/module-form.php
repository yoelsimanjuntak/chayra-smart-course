<form id="form-doc" action="<?=current_url()?>" enctype="multipart/form-data">
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        <label>Kategori</label>
        <select class="form-control" name="<?=COL_IDKATEGORI?>" style="width: 100%">
          <?=GetCombobox("select * from mkategori order by Kategori", COL_UNIQ, COL_KATEGORI, (!empty($data)?$data[COL_IDKATEGORI]:null))?>
        </select>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label>Sub Kategori</label>
        <input type="text" class="form-control" name="<?=COL_MODTITLE?>" value="<?=!empty($data)?$data[COL_MODTITLE]:''?>" />
      </div>
    </div>
  </div>

  <div class="form-group">
    <label>Keterangan</label>
    <textarea class="form-control" rows="4" name="<?=COL_MODDESC?>"><?=!empty($data)?$data[COL_MODDESC]:''?></textarea>
  </div>

  <?php
  if(empty($data)) {
    ?>
    <div class="form-group">
      <label>Dokumen</label>
      <div id="div-attachment">
        <div class="input-group mb-2">
          <div class="input-group-prepend">
            <span class="input-group-text"><i class="fad fa-paperclip"></i></span>
          </div>
          <div class="custom-file">
            <input type="file" class="custom-file-input" name="file" accept="application/pdf">
            <label class="custom-file-label" for="file">PILIH FILE</label>
          </div>
        </div>
        <p class="text-sm text-muted mb-0 font-italic">
          <strong>CATATAN:</strong><br />
          - Besar file / dokumen maksimum <strong>50 MB</strong><br />
          - Jenis file / dokumen yang diperbolehkan hanya dalam format <strong>PDF</strong>
        </p>
      </div>
    </div>
    <?php
  }
  ?>
</form>

<script type="text/javascript">
$(document).ready(function(){
  bsCustomFileInput.init();
  $("select", $('#form-doc')).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  $('#form-doc').validate({
    ignore: "input[type='file']",
    submitHandler: function(form) {
      var modal = $(form).closest('.modal');
      var btnSubmit = null;
      var txtSubmit = '';
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.html();
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            $('.btn-refresh-data').click();
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
          $(form).closest('.modal').modal('hide');
        }
      });

      return false;
    }
  });
});
</script>
