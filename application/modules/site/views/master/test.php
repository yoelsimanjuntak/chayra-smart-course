<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 text-right">
        <a href="<?=site_url('site/master/test-add')?>" class="btn btn-sm btn-primary btn-popup-form" data-title="Tambah"><i class="far fa-plus-circle"></i> TAMBAH</a>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="form-group">
      <div class="input-group mb-3">
        <input type="text" name="filter" class="form-control" placeholder="Pencarian...">
        <div class="input-group-append">
          <span class="input-group-text"><i class="far fa-search"></i></span>
        </div>
      </div>
    </div>
    <div class="row">
      <?php
      if(!empty($data)) {
        foreach($data as $dat) {
          $numDetail = $this->db->where(COL_IDTEST, $dat[COL_UNIQ])->get(TBL_MTESTDETAIL)->num_rows();
          ?>
          <div class="col-md-6 card-data" data-name="<?=strtolower($dat[COL_TESTNAME])?>">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title font-weight-bold"><?=$dat[COL_TESTNAME]?></h3>
              </div>
              <div class="card-body p-0">
                  <table class="table table-striped">
                  <tr>
                    <td style="width: 10px !important; white-space: nowrap">TIPE</td>
                    <td style="width: 10px !important; white-space: nowrap">:</td>
                    <td>
                      <strong><?=$dat[COL_TESTTYPE]=='MUL'?'UMUM':$dat[COL_TESTTYPE]?></strong><?=!empty($dat[COL_TESTREMARKS1])?' / <strong>'.$dat[COL_TESTREMARKS1].'</strong>':''?>
                      <?php
                      if($dat[COL_TESTTYPE]=='MUL' && !empty($dat[COL_TESTOPTION])) {
                        $arrOpts = json_decode($dat[COL_TESTOPTION]);
                        $arrOpts_ = array();
                        foreach($arrOpts as $opt) {
                          $arrOpts_[]=$opt->Opt;
                        }
                        if(!empty($arrOpts_)) {
                          echo '<small>('.implode(",", $arrOpts_).')</small>';
                        }
                      }
                      ?>
                    </td>
                  </tr>
                  <tr>
                    <td style="width: 10px !important; white-space: nowrap">JLH. SOAL / DURASI</td>
                    <td style="width: 10px !important; white-space: nowrap">:</td>
                    <td><strong><?=number_format($dat[COL_TESTQUESTNUM]*(!empty($dat[COL_TESTQUESTSUB])?$dat[COL_TESTQUESTSUB]:1))?></strong> / <strong><?=number_format($dat[COL_TESTDURATION]*(!empty($dat[COL_TESTQUESTSUB])?$dat[COL_TESTQUESTNUM]:1),2)?></strong> <small>(MENIT)</small></td>
                  </tr>
                  <!--<tr>
                    <td style="width: 10px !important; white-space: nowrap">JLH. SOAL TERDAFTAR</td>
                    <td style="width: 10px !important; white-space: nowrap">:</td>
                    <td><strong><?=number_format($dat[COL_TESTQUESTNUM])?></strong></td>
                  </tr>-->
                  <!--<tr>
                    <td style="width: 10px !important; white-space: nowrap">INSTRUKSI / PETUNJUK</td>
                    <td style="width: 10px !important; white-space: nowrap">:</td>
                    <td class="font-weight-bold"><?=$dat[COL_TESTINSTRUCTION]?></td>
                  </tr>-->
                </table>
              </div>
              <div class="card-footer">
                <div class="row">
                  <div class="col-sm-4">
                    <a href="<?=site_url('site/master/test-edit/'.$dat[COL_UNIQ])?>" class="btn btn-block btn-sm btn-success btn-popup-form" data-title="Ubah"><i class="far fa-cog"></i>&nbsp;UBAH</a>
                  </div>
                  <div class="col-sm-4">
                    <a href="<?=site_url('site/master/test-delete/'.$dat[COL_UNIQ])?>" class="btn btn-block btn-sm btn-danger btn-action" data-prompt="Apakah anda yakin ingin menghapus?"><i class="far fa-trash"></i>&nbsp;HAPUS</a>
                  </div>
                  <?php
                  if($dat[COL_TESTTYPE]!='ACR'&&$dat[COL_TESTTYPE]!='PAULI') {
                    ?>
                    <div class="col-sm-4">
                      <a href="<?=site_url('site/master/question/'.$dat[COL_UNIQ])?>" class="btn btn-block btn-sm btn-primary"><i class="far fa-list"></i>&nbsp;KELOLA PAKET SOAL [<strong><?=number_format($numDetail)?></strong>]</a>
                    </div>
                    <?php
                  }
                  ?>
                </div>
              </div>
            </div>
          </div>
          <?php
        }
      } else {
        ?>
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <p class="text-center mb-0 font-italic">
                BELUM ADA DATA TERSEDIA
              </p>
            </div>
          </div>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var modalForm = $('#modal-form');
$(document).ready(function() {
  modalForm.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalForm).empty();
    $('.modal-title', modalForm).html('');
  });

  $('.btn-popup-form').click(function() {
    var url = $(this).attr('href');
    var title = $(this).data('title');
    if(url) {
      if(title) {
        $('.modal-title', modalForm).html(title);
      }

      modalForm.modal('show');
      $('.modal-body', modalForm).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
      $('.modal-body', modalForm).load(url, function(){
        $('button[type=submit]', modalForm).unbind('click').click(function(){
          $('form', modalForm).submit();
        });
      });
    }

    return false;
  });

  $('.btn-action').click(function() {
    var url = $(this).attr('href');
    var prompt = $(this).data('prompt');
    if(confirm((prompt||'Apakah anda yakin?'))) {
      $.get(url, function(res) {
        if(res.error != 0) {
          toastr.error(res.error);
        } else {
          toastr.success(res.success);
        }
      }, "json").done(function() {
        location.reload();
      }).fail(function() {
        toastr.error('SERVER ERROR');
      });
    }

    return false;
  });

  $('[name=filter]').keyup(function(){
    var key = $('[name=filter]').val();
    if(key) {
      $('.card-data').hide();
      $('.card-data[data-name*="'+key.toLowerCase()+'"]').show();
    } else {
      $('.card-data').show();
    }
  });
});
</script>
