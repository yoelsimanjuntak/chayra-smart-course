<?php
$htmlTitle = $this->setting_web_name;
$htmlLogo = base_url().$this->setting_web_logo;
?>
<html>
<head>
  <title><?=$this->setting_web_name.' - '.$rtest[COL_TESTNAME]?></title>
  <style>
  body {
    font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
  }
  th, td {
    padding: 5px;
  }
  table {
    width: 100%;
    border-collapse: collapse;
  }
  table, th, td {
    /*border: 1px solid black !important;*/
  }
  </style>
</head>
<body>
  <table width="100%" style="border-bottom: 0.25px solid #000; margin-bottom: 15px !important">
    <tr>
      <td style="width: 10px; white-space: nowrap">NAMA TEST</td>
      <td style="width: 10px">:</td>
      <td><strong><?=$rtest[COL_TESTNAME]?></strong></td>
    </tr>
    <tr>
      <td style="width: 10px; white-space: nowrap">JUMLAH</td>
      <td style="width: 10px">:</td>
      <td><strong><?=count($rquest)?></strong></td>
    </tr>
  </table>
  <div style="width: 100%">
    <?php
    $rtestCurrent = $rtest;
    $uniqlast = 0;
    $txt_ = '';
    $txtGrp_ = 0;
    for($nq=0; $nq<count($rquest); $nq++) {
      $q = $rquest[$nq];
      $opts = json_decode($q[COL_QUESTOPTION]);

      if($rtestCurrent[COL_TESTTYPE]=='ACR') {
        if($txt_ != $q[COL_QUESTTEXT]) {
          $txtGrp_++;
          $txt_ = $q[COL_QUESTTEXT];
        }
      }
      ?>
      <table style="margin-bottom: 10px">
        <tr>
          <td style="width: 15px; font-weight: bold; text-align: right; vertical-align: top"><?=($nq+1)?>.</td>
          <td>
            <?php
            if(!empty($q[COL_QUESTIMAGE])) {
              $arrImg = explode(",", $q[COL_QUESTIMAGE]);
              if(count($arrImg)>0) {
                foreach($arrImg as $img) {
                  ?>
                  <img class="p-2" src="<?=MY_UPLOADURL.$img?>" style="max-width: 50%" />
                  <?php
                }
              }
            }
            ?>
            <?=$q[COL_QUESTTEXT]?>
            <?php
            if($q[COL_QUESTTYPE]=='MUL') {
              ?>
              <div style="margin-top: .5rem">
                <table style="width: 100% !important">
                  <?php
                  $optHtml = '';
                  $optMax = 0;
                  foreach($opts as $opt) {
                    if(toNum($opt->Val)>$optMax) $optMax = toNum($opt->Val);
                  }
                  foreach($opts as $opt) {
                    ?>
                    <tr>
                      <td style="width: 10px !important; text-align: right; vertical-align: top">
                        <span <?=toNum($opt->Val)==$optMax&&$mode=='withkey'?'style="font-weight: bold"':''?>><?=$opt->Opt.'.'?></span>
                      </td>
                      <td>
                        <span <?=toNum($opt->Val)==$optMax&&$mode=='withkey'?'style="font-weight: bold"':''?>><?=$opt->Txt?>
                          <?php
                          if($mode=='withkey') {
                            ?>
                            &nbsp;<small style="font-size: 7pt; !important; font-style: italic">(Bobot : <?=toNum($opt->Val)?>)</small>
                            <?php
                          }
                          ?>
                        </span>
                      </td>
                    </tr>
                    <?php
                  }
                  ?>
                </table>
              </div>
              <?php
            }
            ?>
          </td>
        </tr>
      </table>
      <?php
    }
    ?>
  </div>
</body>
</html>
