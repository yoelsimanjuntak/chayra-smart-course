<!DOCTYPE html>
<html>
  <head>
    <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>

    <link href="https://nightly.datatables.net/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
    <script src="https://nightly.datatables.net/js/jquery.dataTables.js"></script>

    <link href="https://nightly.datatables.net/scroller/css/scroller.dataTables.css?_=cd11977a9e85e84b9a1ebeb03f7b1a10.css" rel="stylesheet" type="text/css" />
    <script src="https://nightly.datatables.net/scroller/js/dataTables.scroller.js?_=cd11977a9e85e84b9a1ebeb03f7b1a10"></script>
    <meta charset=utf-8 />
    <title>DataTables - JS Bin</title>
    <style>
    body {
      font: 90%/1.45em "Helvetica Neue", HelveticaNeue, Verdana, Arial, Helvetica, sans-serif;
      margin: 0;
      padding: 0;
      color: #333;
      background-color: #fff;
    }
    </style>
  </head>
  <body>
    <div class="container">
      <form id="dataform" method="post" action="#">
        <table id="datalist" class="table table-bordered table-hover table-condensed">
          <thead>
            <tr>
              <th class="text-center" style="width: 10px">#</th>
              <th>ID</th>
              <th>PERTANYAAN</th>
              <th>OPSI</th>
              <th>GAMBAR</th>
              <th>STATUS</th>
              <th>DIBUAT PADA</th>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
      </form>
    </div>
    <script type="text/javascript">
    var dt = null;
    $(document).ready( function () {
			dt = $('#datalist').DataTable({
        "autoWidth" : false,
        "processing": true,
        "serverSide": true,
        "ajax": {
          "url": "<?=site_url('site/master/question-load/'.(isset($idTest)?$idTest:999))?>",
          "type": 'POST',
          "data": function(data){
            data.filterStatus = $('[name=filterStatus]', $('.filtering')).val();
           }
        },
				deferRender: true,
				scrollY: 200,
				scrollCollapse: true,
				scroller: true
			});

			//table.row(12).scrollTo(false);
} );

    </script>
  </body>
</html>
