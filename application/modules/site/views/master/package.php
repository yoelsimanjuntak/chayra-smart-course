<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 text-right">
        <a href="<?=site_url('site/master/package-add')?>" class="btn btn-sm btn-primary btn-popup-form" data-title="Tambah"><i class="far fa-plus-circle"></i> TAMBAH</a>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="form-group">
      <div class="input-group mb-3">
        <input type="text" name="filter" class="form-control" placeholder="Pencarian...">
        <div class="input-group-append">
          <span class="input-group-text"><i class="far fa-search"></i></span>
        </div>
      </div>
    </div>
    <div class="row align-items-stretch">
      <?php
      if(!empty($data)) {
        foreach($data as $dat) {
          $arrItems = array();
          if(!empty($dat[COL_PKGITEMS])) {
            $arrItems = json_decode($dat[COL_PKGITEMS]);
          }
          ?>
          <div class="col-12 col-sm-6 d-flex align-items-stretch card-data" data-name="<?=strtolower($dat[COL_PKGNAME])?>" data-kat="<?=strtolower($dat[COL_KATEGORI])?>">
            <div class="card w-100">
              <div class="card-header">
                <h3 class="card-title font-weight-bold"><?=$dat[COL_PKGNAME]?></h3>
                <div class="card-tools">
                  <a href="<?=site_url('site/master/package-edit/'.$dat[COL_UNIQ])?>" class="btn-tool text-success btn-popup-form " data-title="Ubah"><i class="far fa-cog"></i> UBAH</a>
                  <!--<a href="<?=site_url('site/master/package-formula/'.$dat[COL_UNIQ])?>" class="btn-tool text-primary btn-popup-form " data-title="Formula"><i class="far fa-badge-check"></i> PENILAIAN</a>-->
                </div>
              </div>
              <div class="card-body p-0">
                <table class="table table-bordered" width="100%">
                  <tbody>
                    <tr>
                      <td class="nowrap" style="width: 10px">KATEGORI</td>
                      <td class="nowrap" style="width: 10px">:</td>
                      <td colspan="2" <?=!empty($dat[COL_KATEGORI])&&$dat[COL_KATEGORIISAKTIF]==0?'style="text-decoration: line-through"':'-'?>><?=!empty($dat[COL_KATEGORI])?$dat[COL_KATEGORI]:'-'?></td>
                    </tr>
                    <tr>
                      <td class="nowrap" style="width: 10px">STATUS</td>
                      <td class="nowrap" style="width: 10px">:</td>
                      <td colspan="2">
                        <span class="badge badge-<?=$dat[COL_PKGISACTIVE]==1?'success':'danger'?>"><?=$dat[COL_PKGISACTIVE]==1?'AKTIF':'INAKTIF'?></span>
                      </td>
                    </tr>
                    <tr>
                      <td class="nowrap" style="width: 10px">HARGA</td>
                      <td class="nowrap" style="width: 10px">:</td>
                      <td colspan="2">Rp. <?=number_format($dat[COL_PKGPRICE])?></td>
                    </tr>
                    <?php
                    foreach($arrItems as $i) {
                      $rtest = $this->db
                      ->where(COL_UNIQ, $i->TestID)
                      ->get(TBL_MTEST)
                      ->row_array();
                      ?>
                      <tr>
                        <td class="nowrap text-right font-italic" style="width: 10px"><?=$i->Seq?>.</td>
                        <td class="font-italic" colspan="2"><?=!empty($rtest)?strtoupper($rtest[COL_TESTNAME]):strtoupper($i->TestName)?></td>
                        <td class="nowrap text-center" style="width: 10px">
                          <?php
                          if($rtest[COL_TESTREMARKS1]!='EPPS' && $rtest[COL_TESTREMARKS1]!='IST') {
                            ?>
                            <a href="<?=site_url('site/master/package-formula/'.$dat[COL_UNIQ].'/'.$i->TestID)?>" class="btn btn-xs btn-outline-primary btn-popup-form " data-title="Tabel Penilaian" data-opt-footer="0"><i class="far fa-badge-check"></i> PENILAIAN</a>
                            <?php
                          }
                          ?>
                        </td>
                      </tr>
                      <?php
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <?php
        }
      } else {
        ?>
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <p class="text-center mb-0 font-italic">
                BELUM ADA DATA TERSEDIA
              </p>
            </div>
          </div>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="overlay d-none justify-content-center align-items-center">
        <i class="fas fa-2x fa-spinner fa-spin"></i>
      </div>
        <div class="modal-header">
          <h5 class="modal-title"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer d-block">
          <div class="row">
            <div class="col-lg-12 text-center">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
              <button type="submit" class="btn btn-primary"><i class="far fa-plus-circle"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var modalForm = $('#modal-form');
$(document).ready(function() {
  modalForm.on('hidden.bs.modal', function (e) {
    $('.modal-body', modalForm).empty();
    $('.modal-title', modalForm).html('');
  });

  $('.btn-popup-form').click(function() {
    var url = $(this).attr('href');
    var title = $(this).data('title');
    var optFooter = $(this).data('opt-footer');
    if(url) {
      if(title) {
        $('.modal-title', modalForm).html(title);
      }

      if(optFooter=='0') {
        $('.modal-footer', modalForm).removeClass('d-block').addClass('d-none');
      } else {
        $('.modal-footer', modalForm).removeClass('d-none').addClass('d-block');
      }

      modalForm.modal('show');
      $('.modal-body', modalForm).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
      $('.modal-body', modalForm).load(url, function(){
        $('button[type=submit]', modalForm).unbind('click').click(function(){
          $('form', modalForm).submit();
        });
      });
    }

    return false;
  });

  $('[name=filter]').keyup(function(){
    var key = $('[name=filter]').val();
    console.log(key);
    if(key) {
      $('.card-data').removeClass('d-flex').hide();
      $('.card-data[data-name*="'+key.toLowerCase()+'"],.card-data[data-kat*="'+key.toLowerCase()+'"]').addClass('d-flex').show();
    } else {
      $('.card-data').addClass('d-flex').show();
    }
  });
});
</script>
