<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta property="og:title" content="<?=$this->setting_web_desc?>" />
		<meta property="og:type" content="article" />
		<meta property="og:url" content="<?=base_url()?>" />
		<meta property="og:image" content="<?=base_url().$this->setting_web_logo?>" />
    <link rel="icon" type="image/png" href="<?=base_url().$this->setting_web_icon?>">

    <title><?= $this->setting_web_name ?> - <?=$title?></title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />
    <!-- Ionicons -->
    <link href="<?=base_url()?>assets/tbs/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/dist/css/adminlte.min.css">
    <!-- jQuery -->
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script src="<?=base_url()?>assets/js/jquery.particleground.js"></script>

    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.js"></script>
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.css">

    <style>
        .adsbox{
            background: none repeat scroll 0 0 #f5f5f5;
            border-radius: 10px;
            box-shadow: 0 0 5px 1px rgba(50, 50, 50, 0.2);
            margin: 20px auto 0;
            padding: 15px;
            border: 1px solid #caced3;
            height: 145px;
        }
        html, body {
            width: 100%;
            height: 100%;
            overflow: hidden;
        }
        #particles {
            width: 100%;
            height: 100%;
            overflow: hidden;
        }
        #intro {
            position: absolute;
            left: 0;
            top: 50%;
            padding: 0 20px;
            width: 100%;
            #text-align: center;
        }
    </style>
</head>
<!-- Preloader Style -->
<style>
    .no-js #loader { display: none;  }
    .js #loader { display: block; position: absolute; left: 100px; top: 0; }
    .se-pre-con {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('<?=base_url().$this->setting_web_preloader?>') center no-repeat #fff;
    }
</style>
<!-- /.preloader style -->

<!-- Preloader Script -->
<script>
    // Wait for window load
    $(window).load(function() {
        // Animate loader off screen
        $(".se-pre-con").fadeOut("slow");
    });
</script>
<!-- /.preloader script -->
<body class="hold-transition login-page">

<!-- preloader -->
<div class="se-pre-con"></div>
<!-- /.preloader -->

<div class="login-box">
  <div class="card">
    <div class="card-header">
      <h5 class="card-title"><?=$title?></h5>
    </div>
    <div class="card-body login-card-body">
      <p class="m-0"><?=$subtitle?></p>
    </div>
    <div class="card-footer">
      <a href="<?=site_url('site/user/dashboard')?>" class="btn btn-primary btn-block"><i class="far fa-home"></i>&nbsp;KEMBALI</a>
    </div>
  </div>
</div>
</body>
</html>
