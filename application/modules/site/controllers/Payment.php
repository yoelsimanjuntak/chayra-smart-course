<?php
class Payment extends MY_Controller {
  function __construct() {
    parent::__construct();
    if(!IsLogin()) {
      if (!$this->input->is_ajax_request()) {
        redirect('site/user/login');
      } else {
        ShowJsonError('HARAP LOGIN TERLEBIH DAHULU!');
        exit();
      }
    }
  }

  public function create($type, $id) {
    $ruser = GetLoggedUser();
    $dtpayment = array();
    $dtpaymentdet = array();
    if($type=='sess') {
      $rpackage = $this->db->where(COL_UNIQ, $id)
      ->get(TBL_MTESTPACKAGE)
      ->row_array();
      if(empty($rpackage)) {
        show_error('Parameter tidak valid!');
        exit();
      }

      $dtpayment = array(
        COL_USERNAME=>$ruser[COL_USERNAME],
        COL_PAYMENTNO=>'CSC-'.date('YmdHis'),
        COL_PAYMENTAMOUNT=>$rpackage[COL_PKGPRICE],
        COL_CREATEDON=>date('Y-m-d H:i:s'),
        COL_CREATEDBY=>$ruser[COL_USERNAME]
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TPAYMENT, $dtpayment);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }

        $idPayment = $this->db->insert_id();
        $dtpaymentdet = array(
          COL_IDPAYMENT=>$idPayment,
          COL_IDREF=>$id,
          COL_TYPE=>$type,
          COL_DESC=>$rpackage[COL_PKGNAME],
          COL_PRICE=>$rpackage[COL_PKGPRICE],
          COL_QTY=>1,
          COL_REMARKS1=>isset($_GET['remarks1'])?$_GET['remarks1']:null,
          COL_REMARKS2=>isset($_GET['remarks2'])?$_GET['remarks2']:null
        );
        $res = $this->db->insert(TBL_TPAYMENTDETAIL, $dtpaymentdet);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }

        $dtorder = array(
          'amount'=>$dtpayment[COL_PAYMENTAMOUNT],
          'invoice_number'=>$dtpayment[COL_PAYMENTNO],
          'currency'=>'IDR',
          'language'=>'ID',
          'callback_url'=>site_url('site/doku/callback-payment/'.$idPayment),
          'callback_url_cancel'=>site_url('site/doku/callback-cancel/'.$idPayment),
          'auto_redirect'=>true,
          'disable_retry_payment'=>true,
          'line_items'=>array(
            array('id'=>$rpackage[COL_UNIQ],'name'=>$rpackage[COL_PKGNAME],'quantity'=>1,'price'=>$rpackage[COL_PKGPRICE])
          ),
        );
        $dtcustomer = array(
          'id'=>$ruser[COL_USERNAME],
          'name'=>$ruser[COL_FULLNAME],
          'email'=>$ruser[COL_EMAIL],
          'country'=>'ID'
        );

        $doku = doku_create_payment($dtorder,$dtcustomer);
        if($doku->message[0]!='SUCCESS' || empty($doku->response) || empty($doku->response->payment->url)) {
          throw new Exception('Transaksi dibatalkan.');
        }

        $this->db->trans_commit();
        redirect($doku->response->payment->url);
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        show_error($ex->getMessage());
        exit();
      }
    } else if($type=='subs') {

    }
  }

  public function index() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('Anda tidak memiliki hak akses!');
      exit();
    }

    $data['title'] = "Daftar Transaksi";
    $this->template->load('adminlte', 'payment/index', $data);
  }

  public function index_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    $dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_CREATEDON);
    $cols = array(COL_PAYMENTNO,COL_PAYMENTSTATUS,COL_PAYMENTVIA);

    $queryAll = $this->db
    ->join(TBL_USERS.' uc','uc.'.COL_USERNAME." = ".TBL_TPAYMENT.".".COL_USERNAME,"left")
    ->get(TBL_TPAYMENT);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($dateFrom)) {
      //$this->db->where(TBL_TPAYMENT.'.'.COL_CREATEDON.' >= ', $dateFrom);
    }
    if(!empty($dateTo)) {
      //$this->db->where(TBL_TPAYMENT.'.'.COL_CREATEDON.' <= ', $dateTo);
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('tpayment.*, uc.Fullname, (select GROUP_CONCAT(det.Desc) from tpaymentdetail det where det.IdPayment=tpayment.Uniq) as Items')
    ->join(TBL_USERS.' uc','uc.'.COL_USERNAME." = ".TBL_TPAYMENT.".".COL_CREATEDBY,"left")
    ->get_compiled_select(TBL_TPAYMENT, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $data[] = array(
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON])),
        $r[COL_PAYMENTNO],
        $r[COL_FULLNAME],
        $r['Items'],
        number_format($r[COL_PAYMENTAMOUNT]),
        $r[COL_PAYMENTSTATUS],
        (!empty($r[COL_PAYMENTVIA]))?$r[COL_PAYMENTVIA]:'-',
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }
}
