<?php
class Doku extends MY_Controller {
  public function callback_payment($id=null) {
    $txtPayment = '';
    $rpayment = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_TPAYMENT)
    ->row_array();
    if(!empty($rpayment)) {
      $txtPayment = $rpayment[COL_PAYMENTNO];
    }

    $data['title'] = 'Pembayaran Selesai';
    $data['subtitle'] = 'Anda telah menyelesaikan transaksi <strong>'.$txtPayment.'</strong>. Silakan kembali ke dashboard akun anda untuk melanjutkan.';
    $this->load->view('site/doku/callback', $data);
  }

  public function callback_cancel($id=null) {
    $txtPayment = '';
    $rpayment = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_TPAYMENT)
    ->row_array();
    if(!empty($rpayment)) {
      $txtPayment = $rpayment[COL_PAYMENTNO];
    }

    $data['title'] = 'Pembayaran Dibatalkan';
    $data['subtitle'] = 'Anda telah membatalkan transaksi. Silakan kembali ke dashboard akun anda untuk melanjutkan.';
    $this->load->view('site/doku/callback', $data);
  }

  public function notif_va() {
    $sapi_type = php_sapi_name();

    $notificationHeader = getallheaders();
    $notificationBody = file_get_contents('php://input');
    $notificationPath = '/site/doku/notif-va';
    $secretKey = DOKU_SECRETKEY;

    $digest = base64_encode(hash('sha256', $notificationBody, true));
    $rawSignature = "Client-Id:".$notificationHeader['Client-Id']."\n"
        ."Request-Id:".$notificationHeader['Request-Id']."\n"
        ."Request-Timestamp:".$notificationHeader['Request-Timestamp']."\n"
        ."Request-Target:".$notificationPath."\n"
        ."Digest:".$digest;

    $signature = base64_encode(hash_hmac('sha256', $rawSignature, $secretKey, true));
    $finalSignature = 'HMACSHA256='.$signature;

    if ($finalSignature == $notificationHeader['Signature']) {
      $resp = json_decode($notificationBody);
      $retval = 'OK';
      if(!empty($resp)) {
        $dtpayment = array();
        if(isset($resp->order) && isset($resp->order->invoice_number)) {
          if(isset($resp->channel) && isset($resp->channel->id)) $dtpayment[COL_PAYMENTVIA] = $resp->channel->id;
          if(isset($resp->transaction) && isset($resp->transaction->status)) {
            $dtpayment[COL_PAYMENTSTATUS] = $resp->transaction->status;
            if($resp->transaction->status == 'SUCCESS' && isset($resp->transaction->date)) {
              $dtpayment[COL_PAYMENTDATE] = date('Y-m-d H:i:s', strtotime($resp->transaction->date));
            }
          }

          if(!empty($dtpayment)) {
            $res = $this->db->where(COL_PAYMENTNO, $resp->order->invoice_number)->update(TBL_TPAYMENT, $dtpayment);
            if(!$res) {
              $retval = $err['message'];
            }

            if(isset($dtpayment[COL_PAYMENTSTATUS]) && $dtpayment[COL_PAYMENTSTATUS]=='SUCCESS') {
              $rpayment = $this->db
              ->where(COL_PAYMENTNO, $resp->order->invoice_number)
              ->get(TBL_TPAYMENT)
              ->row_array();

              if(!empty($rpayment)) {
                $rdet = $this->db
                ->where(COL_IDPAYMENT, $rpayment[COL_UNIQ])
                ->get(TBL_TPAYMENTDETAIL)
                ->result_array();

                foreach($rdet as $d) {
                  if($d[COL_TYPE]=='sess') {
                    $dat = array(
                      COL_IDPACKAGE=>$d[COL_IDREF],
                      COL_USERNAME=>$rpayment[COL_USERNAME],
                      COL_SESSREMARK1=>$d[COL_REMARKS1],
                      COL_SESSREMARK2=>$d[COL_REMARKS2],
                      COL_CREATEDBY=>$rpayment[COL_USERNAME],
                      COL_CREATEDON=>date('Y-m-d H:i:s')
                    );

                    $res = $this->db->insert(TBL_TSESSION, $dat);
                  }
                }
              }
            }
          }
        }
      }
      //return response($retval, 200)->header('Content-Type', 'text/plain');
      header('Content-Type: text/plain');
      if (substr($sapi_type, 0, 3) == 'cgi') header("Status: 200 ".$retval);
      else header("HTTP/1.1 200 ".$retval);
    } else {
      //return response('Invalid Signature', 500)->header('Content-Type', 'text/plain');
      header('Content-Type: text/plain');
      if (substr($sapi_type, 0, 3) == 'cgi') header("Status: 400 Invalid Signature");
      else header("HTTP/1.1 400 Invalid Signature");
    }
  }

  public function test() {
    $url = 'https://api.doku.com';
    $target = '/checkout/v1/payment';
    $clientId = DOKU_CLIENTID;
    $secretKey = DOKU_SECRETKEY;
    $requestId = 'CSC-'.date('YmdHis');
    $requestTimestamp = date('Y-m-d\TH:i:s\Z', strtotime(date('Y-m-d H:i:s'). '-7 hours'));
    $requestTarget = $target;

    // Req Body
    $dataBody = array(
      'order'=>array(
        'amount'=>2000,
        'invoice_number'=>'csc-'.date('YmdHis'),
        'currency'=>'IDR',
        'callback_url'=>site_url('site/doku/callback-complete'),
        'callback_url_cancel'=>site_url('site/doku/callback-cancel'),
        'auto_redirect'=>true,
        'disable_retry_payment'=>true,
        'line_items'=>array(
          array('id'=>'001','name'=>'Paket 01','quantity'=>1,'price'=>2000)
        ),
      ),
      'payment'=>array(
        'payment_due_date'=>60,
        'payment_method_types'=>array(
          "QRIS",
          "EMONEY_OVO",
          "EMONEY_SHOPEE_PAY",
          "VIRTUAL_ACCOUNT_BCA",
          "VIRTUAL_ACCOUNT_BANK_MANDIRI",
          "VIRTUAL_ACCOUNT_BANK_SYARIAH_MANDIRI",
          "VIRTUAL_ACCOUNT_DOKU",
          "VIRTUAL_ACCOUNT_BRI",
          "VIRTUAL_ACCOUNT_BNI",
          "VIRTUAL_ACCOUNT_BANK_PERMATA",
          "VIRTUAL_ACCOUNT_BANK_CIMB",
          "VIRTUAL_ACCOUNT_BANK_DANAMON",
        )
      ),
      'customer'=>array(
        'id'=>'rolassimanjuntak@gmail.com',
        'name'=>'Rolas',
        'last_name'=>'Simanjuntak',
        'phone'=>'6285359867032',
        'email'=>'rolassimanjuntak@gmail.com',
        'address'=>'Medan',
        'postcode'=>'',
        'state'=>'Sumatera Utara',
        'city'=>'Medan',
        'country'=>'ID'
      )
    );

    // Signature
    $digest = base64_encode(hash('sha256', json_encode($dataBody), true));
    $rawSignature = "Client-Id:".$clientId."\n".
    "Request-Id:".$requestId."\n".
    "Request-Timestamp:".$requestTimestamp."\n".
    "Request-Target:".$requestTarget."\n".
    "Digest:".$digest;

    $signature = base64_encode(hash_hmac('sha256', $rawSignature, $secretKey, true));
    $finalSignature = 'HMACSHA256='.$signature;

    $dataHeader = [
      'Content-Type: application/json',
      'Client-Id:'.$clientId,
      'Request-Id:'.$requestId,
      'Request-Timestamp:'.$requestTimestamp,
      'Signature:'.$finalSignature
    ];

    $curl = curl_init($url.$target);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $dataHeader);
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($dataBody));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($curl);
    curl_close($curl);
    echo json_encode($response);
  }
}
?>
