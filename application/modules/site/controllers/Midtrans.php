<?php
require_once './vendor/midtrans/midtrans-php/Midtrans.php';
class Midtrans extends MY_Controller {
  public function callback_notification() {
    \Midtrans\Config::$serverKey = 'SB-Mid-server-sMsrrBp9-sHVNP3NO1MWl7Z6';
    \Midtrans\Config::$isProduction = false;
    \Midtrans\Config::$isSanitized = true;
    \Midtrans\Config::$is3ds = true;
    $notif = new Notification();

    $transaction = $notif->transaction_status;
    $type = $notif->payment_type;
    $order_id = $notif->order_id;
    $fraud = $notif->fraud_status;

    if ($transaction == 'capture') {
        // For credit card transaction, we need to check whether transaction is challenge by FDS or not
        if ($type == 'credit_card') {
            if ($fraud == 'challenge') {
                // TODO set payment status in merchant's database to 'Challenge by FDS'
                // TODO merchant should decide whether this transaction is authorized or not in MAP
                echo "Transaction order_id: " . $order_id ." is challenged by FDS";
            } else {
                // TODO set payment status in merchant's database to 'Success'
                echo "Transaction order_id: " . $order_id ." successfully captured using " . $type;
            }
        }
    } else if ($transaction == 'settlement') {
        // TODO set payment status in merchant's database to 'Settlement'
        echo "Transaction order_id: " . $order_id ." successfully transfered using " . $type;
    } else if ($transaction == 'pending') {
        // TODO set payment status in merchant's database to 'Pending'
        echo "Waiting customer to finish transaction order_id: " . $order_id . " using " . $type;
    } else if ($transaction == 'deny') {
        // TODO set payment status in merchant's database to 'Denied'
        echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is denied.";
    } else if ($transaction == 'expire') {
        // TODO set payment status in merchant's database to 'expire'
        echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is expired.";
    } else if ($transaction == 'cancel') {
        // TODO set payment status in merchant's database to 'Denied'
        echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is canceled.";
    }
  }

  public function callback_vt_finish() {
    $orderid = $_GET['order_id'];
    $statuscode = $_GET['status_code'];
    $status = $_GET['transaction_status'];

    echo $orderid.' // '.$statuscode.' // '.$status;
  }

  public function test() {
    \Midtrans\Config::$serverKey = 'SB-Mid-server-sMsrrBp9-sHVNP3NO1MWl7Z6';
    \Midtrans\Config::$isProduction = false;
    \Midtrans\Config::$isSanitized = true;
    \Midtrans\Config::$is3ds = true;

    $params = array(
      'transaction_details' => array(
          'order_id' => rand(),
          'gross_amount' => 10000,
      )
    );

    try {
      // Get Snap Payment Page URL
      $paymentUrl = \Midtrans\Snap::createTransaction($params)->redirect_url;

      // Redirect to Snap Payment Page
      header('Location: ' . $paymentUrl);
    }
    catch (Exception $e) {
      echo $e->getMessage();
    }
  }
}
?>
